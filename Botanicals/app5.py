from flask import Flask, render_template, request, jsonify, flash, session, send_file, redirect
import pandas as pd
import os
import csv
from werkzeug.utils import secure_filename
import plotly.graph_objects as go
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np

import logging
app = Flask(__name__)
app.secret_key = 'your_secret_key'
UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'csv'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# Get the current directory
current_dir = os.path.dirname(os.path.realpath(__file__))
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/target')
def target():
    return render_template('compounds.html')

# Load CSV data into memory for hits search
csv_file_path_hits = '/home/srinivasan/Desktop/databse_testing_3/updated_1-501.csv'
data_hits = pd.read_csv(csv_file_path_hits)

# Load CSV data into memory for compound search
csv_file_path_compound = '/home/srinivasan/Desktop/pathways_testing_new_botanical_database/Testing_on_redshift/target_merged_file_2.csv'
data_compound = pd.read_csv(csv_file_path_compound)

@app.route('/search', methods=['POST'])
def search_data_hits():
    try:
        search_term = request.get_json()['searchTerm'].lower()  # Convert search term to lowercase

        # Convert 'Compound' and 'Compounds SMILES' column values to lowercase for case-insensitive comparison
        data_hits['Compound'] = data_hits['Compound'].str.lower()
        data_hits['Compounds SMILES'] = data_hits['Compounds SMILES'].str.lower()

        # Ensure correct column names for searching from your CSV file (modify if needed)
        filtered_data = data_hits[(data_hits['Compound'] == search_term) | (data_hits['Compounds SMILES'] == search_term)]

        if filtered_data.empty:
            return jsonify({'error': 'No data found for the compound name.'}), 404

        # Sort the filtered data by scores in descending order
        filtered_data = filtered_data.sort_values(by='Score', ascending=False)
        filtered_data = filtered_data.fillna('')
        targets = filtered_data['Target'].tolist()
        scores = filtered_data['Score'].tolist()

        # Create the bar graph using Plotly
        fig = go.Figure(data=[go.Bar(x=targets, y=scores)])
        fig.update_layout(title='Target vs. Score for Compound: ' + search_term,
                          xaxis_title='Target', yaxis_title='Score')
        graph_json = fig.to_json()

        # Prepare data for table
        table_columns = ['Target', 'UniProt ID', 'Score', 
                         'Most Similar Compound', 'Compounds SMILES', 
                         'Compound FP2 Similarity', 'Compound Pharmacophore Similarity', 
                         'Assay Value', 'Assay Type']
        table_data = filtered_data[table_columns].to_dict('records')

        return jsonify({'targets': targets, 'scores': scores, 'graph': graph_json, 'table': table_data})

    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/searchtarget', methods=['POST'])
def search_data_compound():
    try:
        search_term = request.get_json()['searchTerm'].lower()  # Convert search term to lowercase

        # Convert 'Compound' column values to lowercase for case-insensitive comparison
        data_compound['Compound'] = data_compound['Compound'].str.lower()

        # Ensure correct column names for searching from your CSV file (modify if needed)
        filtered_data = data_compound[(data_compound['Compound'] == search_term)]

        if filtered_data.empty:
            return jsonify({'error': 'No data found for the compound name.'}), 404

        # Sort the filtered data by scores in descending order
        filtered_data = filtered_data.sort_values(by='Score', ascending=False)
        filtered_data = filtered_data.fillna('')
        targets = filtered_data['Target'].tolist()
        scores = filtered_data['Score'].tolist()

        # Create the bar graph using Plotly
        fig = go.Figure(data=[go.Bar(x=targets, y=scores)])
        fig.update_layout(title='Compounds vs. Score for Target: ' + search_term,
                          xaxis_title='Compounds', yaxis_title='Score')
        graph_json = fig.to_json()

        # Prepare data for table
        table_columns = ['Target', 'Compound', 'Score']
        table_data = filtered_data[table_columns].to_dict('records')

        return jsonify({'targets': targets, 'scores': scores, 'graph': graph_json, 'table': table_data})

    except Exception as e:
        return jsonify({'error': str(e)}), 500

#plants 
# Load compound information (name, SMILES, taxonomy)
compound_info_csv_file = "/home/srinivasan/Desktop/pathways_testing_new_botanical_database/Testing_on_redshift/combined.csv"
compound_data = pd.read_csv(compound_info_csv_file)
print(compound_data)
# plate data tab 
# Load your CSV file containing compound names, SMILES, and taxonomy
plant_csv_file = "/home/srinivasan/Desktop/pathways_testing_new_botanical_database/Testing_on_redshift/compound_mapping2.csv"
compound_info= pd.read_csv(plant_csv_file)    



# Configure logging
logging.basicConfig(level=logging.DEBUG)

@app.route('/searchplant', methods=['POST'])
def search_plant_data():
    try:
        search_term = request.get_json()['searchTerm'].lower()  # Convert search term to lowercase

        # Filter compound information based on the provided taxonomy
        filtered_compound_info = compound_info[compound_info['Taxonomy'].str.lower() == search_term]
        if filtered_compound_info.empty:
            return jsonify({'error': 'No compounds found for the provided taxonomy.'}), 404

        # Get the list of compound names for the filtered compounds
        compound_names = filtered_compound_info['Compound Name']

        # Filter compound data based on the list of compound names
        filtered_compound_data = compound_data[compound_names]

        # Extract target names from the first column of compound_data
        target_names = compound_data.iloc[:, 0]

        # Initialize lists to store top target and score for each compound
        top_targets = []
        top_scores = []

        # Iterate through each compound
        for compound in compound_names:
            # Find the target with the maximum score for the current compound
            max_score = filtered_compound_data[compound].max()
            max_target_index = filtered_compound_data[compound].idxmax()
            max_target_name = target_names[max_target_index]

            # Append top target and score to the respective lists
            top_targets.append(max_target_name)
            top_scores.append(max_score)

        # Create a new DataFrame with compound name, SMILES, and the highest scoring target along with its score
        result_table = pd.DataFrame({
            'Compound Name': compound_names,
            'SMILES': filtered_compound_info['SMILES'],
            'Top Target': top_targets,
            'Score': top_scores
        })

        # Convert DataFrame to dictionary
        table_data = result_table.to_dict('records')

        return jsonify({'table': table_data})
    except Exception as e:
        logging.error(f"Error occurred: {str(e)}")
        return jsonify({'error': str(e)}), 500

  #auto complete_funtions


#Formulation Webpage 
csv_file = "/home/srinivasan/Desktop/pathways_testing_new_botanical_database/Testing_on_redshift/combined.csv"
data = pd.read_csv(csv_file, index_col=0)  # Assuming index_col=0

# Load your CSV file containing compound names, SMILES, and taxonomy
smiles_csv_file = "/home/srinivasan/Desktop/pathways_testing_new_botanical_database/Testing_on_redshift/compound_mapping2.csv"
smiles_data = pd.read_csv(smiles_csv_file)

def calculate_batch_with_column_name(query_value_pairs):
    results = {}
    for query, value in query_value_pairs:
        try:
            value = float(value)
        except ValueError:
            return "Please provide a valid numerical value"

        if query not in data.columns:
            return "Invalid query: {}".format(query)

        targets = dict(data[query] * value)
        for target, score in targets.items():
            if target in results:
                results[target] += score
            else:
                results[target] = score

    # Filter out targets with score 0
    results = {k: v for k, v in results.items() if v != 0}

    # Normalize the scores
    scores = list(results.values())
    scaler = MinMaxScaler()
    normalized_scores = scaler.fit_transform(pd.DataFrame(scores))

    # Update results dictionary with normalized scores
    normalized_results = {target: normalized_score[0] for target, normalized_score in zip(results.keys(), normalized_scores)}

    # Sort the normalized results
    sorted_results = sorted(normalized_results.items(), key=lambda x: x[1], reverse=True)
    return sorted_results

def calculate_batch_with_smiles(smiles_value_pairs):
    results = {}
    for smiles, value in smiles_value_pairs:
        matching_rows = smiles_data[smiles_data['SMILES'] == smiles]
        if matching_rows.empty:
            return "No compounds found for SMILES: {}".format(smiles)

        for _, row in matching_rows.iterrows():
            compound_name = row['Compound Name']
            if compound_name not in data.columns:
                continue  # Skip if compound name not found in data

            try:
                value = float(value)
            except ValueError:
                return "Invalid value for SMILES: {}".format(value)

            targets = dict(data[compound_name] * value)
            for target, score in targets.items():
                if target in results:
                    results[target] += score
                else:
                    results[target] = score

    # Filter out targets with score 0
    results = {k: v for k, v in results.items() if v != 0}

    if results:
        # Normalize the scores if there are results
        scores = list(results.values())
        scaler = MinMaxScaler()
        normalized_scores = scaler.fit_transform(pd.DataFrame(scores))

        # Update results dictionary with normalized scores
        normalized_results = {target: normalized_score[0] for target, normalized_score in zip(results.keys(), normalized_scores)}

        # Sort the normalized results
        sorted_results = sorted(normalized_results.items(), key=lambda x: x[1], reverse=True)
        return sorted_results
    else:
        return "No results found for given SMILES"


# Function to calculate using taxonomy name for batch
def calculate_batch_with_taxonomy(taxonomy_value_pairs):
    results = {}
    for taxonomy, value in taxonomy_value_pairs:
        matching_rows = smiles_data[smiles_data['Taxonomy'] == taxonomy]
        if matching_rows.empty:
            return "No compounds found for taxonomy: {}".format(taxonomy)

        for _, row in matching_rows.iterrows():
            compound_name = row['Compound Name']
            if compound_name not in data.columns:
                continue  # Skip if compound name not found in data
            targets = dict(data[compound_name] * float(value))
            for target, score in targets.items():
                if target in results:
                    results[target] += score
                else:
                    results[target] = score

    # Filter out targets with score 0
    results = {k: v for k, v in results.items() if v != 0}

    # Normalize the scores
    scores = list(results.values())
    scaler = MinMaxScaler()
    normalized_scores = scaler.fit_transform(pd.DataFrame(scores))

    # Update results dictionary with normalized scores
    normalized_results = {target: normalized_score[0] for target, normalized_score in zip(results.keys(), normalized_scores)}

    # Sort the normalized results
    sorted_results = sorted(normalized_results.items(), key=lambda x: x[1], reverse=True)
    return sorted_results

@app.route('/search-formulations', methods=['POST'])
def calculate_batch():
    try:
        batch_data = request.form['batch'] 

        if not batch_data.strip():
            return "Please provide batch data", 400

        # Parse batch data to extract compound names/SMILES/taxonomy and values
        lines = batch_data.strip().split('\n')
        query_value_pairs = []
        for line in lines:
            parts = line.strip().split('\t')
            if len(parts) == 2:
                query = parts[0].strip()
                value = parts[1].strip()
                query_value_pairs.append((query, value))

        smiles_queries = [query for query, _ in query_value_pairs if query in smiles_data['SMILES'].values]
        taxonomy_queries = [query for query, _ in query_value_pairs if query in smiles_data['Taxonomy'].values]
        print(taxonomy_queries)
        compound_name_queries = [query for query, _ in query_value_pairs if query in data.columns]
        print(compound_name_queries)
        if len(smiles_queries) + len(taxonomy_queries) + len(compound_name_queries) != len(query_value_pairs):
            return "Invalid queries. Please provide valid SMILES, taxonomy names, or compound names."

        results = {}
        if smiles_queries:
            results.update(calculate_batch_with_smiles([(query, value) for query, value in query_value_pairs if query in smiles_queries]))
        if taxonomy_queries:
            results.update(calculate_batch_with_taxonomy([(query, value) for query, value in query_value_pairs if query in taxonomy_queries]))
        if compound_name_queries:
            results.update(calculate_batch_with_column_name([(query, value) for query, value in query_value_pairs if query in compound_name_queries]))

        sorted_results = sorted(results.items(), key=lambda x: x[1], reverse=True)
        print(sorted_results)

        total_value = sum(value for _, value in sorted_results)
        normalized_results = [(key, value / total_value) for key, value in sorted_results]

        return jsonify(sorted_results)


    except KeyError:
        return "Error: Missing 'batch' key in form data", 400



#pathways

from itertools import combinations

# Load the CSV file
df = pd.read_csv('/home/srinivasan/Desktop/pathways_testing_new_botanical_database/Testing_on_redshift/testing_reactome_combined_uniport_chebi.csv')


def assign_inverse_scores(target_uniprot_ids, num_user_ids):
    # Replace NaN values in 'Submitted entities found' column with an empty string
    df['Submitted entities found'] = df['Submitted entities found'].fillna('')
    
    # Filter rows containing all the target UniProt IDs in 'Submitted entities found'
    pathways = df[df['Submitted entities found'].apply(lambda x: all(target_id in x.split(';') for target_id in target_uniprot_ids))]
    
    # Drop rows with missing values in 'Entities pValue' column
    pathways = pathways.dropna(subset=['Entities pValue'])
    
    # Calculate the count of other entities in each pathway
    pathways['Other entities count'] = pathways['Submitted entities found'].apply(lambda x: len([entity for entity in x.split(';') if entity not in target_uniprot_ids]))
    
    # Calculate total entities in the mapped entries
    pathways['Total entities'] = pathways['Submitted entities found'].apply(lambda x: len(x.split(';')) if x else 0)
    
    # Calculate the number of mapped UniProt IDs
    pathways['Mapped IDs'] = pathways['Submitted entities found'].apply(lambda x: sum(target_id in x.split(';') for target_id in target_uniprot_ids))
    
    # Add a column with the names of the UniProt IDs provided by the user
    pathways['User IDs'] = ', '.join(target_uniprot_ids)
    
    # Inverse the count of other entities to get scores
    pathways['Mapped IDs Score'] = pathways['Mapped IDs'] / num_user_ids  # Divide by the total number of UniProt IDs provided by the user
    # print(num_user_ids)
    # print(pathways['Mapped IDs Score'] )
    # Normalize both scores using min-max normalization
    # min_mapped_ids_score = pathways['Mapped IDs Score'].min()
    # max_mapped_ids_score = pathways['Mapped IDs Score'].max()
    # pathways['Mapped IDs Score'] = (pathways['Mapped IDs Score'] - min_mapped_ids_score) / (max_mapped_ids_score - min_mapped_ids_score)
    
    # Combine the scores with specified weights
    pathways['Score'] = pathways['Mapped IDs Score']
    
    # Select relevant columns
    pathways = pathways[['Pathway identifier', 'Pathway name', 'Submitted entities found', 'Entities pValue', 'Total entities', 'Mapped IDs', 'User IDs', 'Score']]
    
    return pathways

def check_combinations(target_uniprot_ids, total_ids):
    # num_user_ids = total_ids
    num_user_ids = total_ids
    # print(num_user_ids)
    all_mapped_pathways = []
    numeric_user_ids_df = pd.DataFrame(columns=['Pathway identifier', 'Pathway name', 'Submitted entities found', 'Entities pValue', 'Total entities', 'Mapped IDs', 'User IDs'])
    alphanumeric_user_ids_df = pd.DataFrame(columns=['Pathway identifier', 'Pathway name', 'Submitted entities found', 'Entities pValue', 'Total entities', 'Mapped IDs', 'User IDs', 'Score'])
    
    for r in range(1, num_user_ids+1):
        id_combinations = [list(comb) for comb in combinations(target_uniprot_ids, r)]
        for combination in id_combinations:
            pathways = assign_inverse_scores(combination, num_user_ids)
            if not pathways.empty:
                if len(combination) == pathways.iloc[0]['Mapped IDs']:
                    if all(char.isdigit() for char in ''.join(combination)):
                        numeric_user_ids_df = pd.concat([numeric_user_ids_df, pathways[['Pathway identifier', 'Pathway name', 'Submitted entities found', 'Entities pValue', 'Total entities', 'Mapped IDs', 'User IDs']]])
                    else:
                        alphanumeric_user_ids_df = pd.concat([alphanumeric_user_ids_df, pathways])
                    all_mapped_pathways.append(pathways)
                else:
                    print(f"Combination {combination} found in the Submitted entities found:")
                    print(pathways)
                    print()
    
    all_pathways_data = pd.concat(all_mapped_pathways)
    all_pathways_data = all_pathways_data.sort_values(by=['Score', 'Mapped IDs'], ascending=[False, False])
    
    common_pathway_names = set(numeric_user_ids_df['Pathway name']).intersection(alphanumeric_user_ids_df['Pathway name'])
    
    common_pathways_numeric = numeric_user_ids_df[numeric_user_ids_df['Pathway name'].isin(common_pathway_names)]
    common_pathways_alphanumeric = alphanumeric_user_ids_df[alphanumeric_user_ids_df['Pathway name'].isin(common_pathway_names)]
    
    common_pathways_data = pd.concat([common_pathways_numeric, common_pathways_alphanumeric])
    
    grouped_pathways = common_pathways_data.groupby(['Pathway identifier', 'Pathway name']).agg({
        'Submitted entities found': lambda x: ', '.join(set(';'.join(x).split(';'))),
        'Entities pValue': 'mean',
        'Total entities': 'sum',
        'Mapped IDs': 'sum',
        'User IDs': lambda x: ', '.join(set(x)),
        'Score': 'sum'
    })

    grouped_pathways.reset_index(inplace=True)

    sorted_pathways = grouped_pathways.sort_values(by='Score', ascending=False)
    numeric_user_ids_df = numeric_user_ids_df.sort_values(by=['Entities pValue', 'Mapped IDs'], ascending=[True, False]) # Sort by pValue ascending, Mapped IDs descending
    alphanumeric_user_ids_df = alphanumeric_user_ids_df.sort_values(by=['Score', 'Mapped IDs'], ascending=[False, False])
    if not numeric_user_ids_df.empty:
        numeric_user_ids_df.to_csv('numeric_user_ids_pathways.csv', index=False)
    if not alphanumeric_user_ids_df.empty:
        alphanumeric_user_ids_df.to_csv('alphanumeric_user_ids_pathways.csv', index=False)
    if not common_pathways_data.empty:
        common_pathways_data.to_csv('common_pathways.csv', index=False)
    
    return numeric_user_ids_df, alphanumeric_user_ids_df, sorted_pathways

from flask import jsonify

@app.route('/search-pathways', methods=['POST'])
def calculate_pathways():
    try:
        target_uniprot_ids = request.form.get('batch').split('\n')
        target_uniprot_ids = [id.strip() for id in target_uniprot_ids if id.strip()]  # Remove empty strings
        # print(target_uniprot_ids)
        total_ids = len(target_uniprot_ids)
        if not target_uniprot_ids:
            return jsonify({"error": "Please provide UniProt IDs"}), 400

        # Process the UniProt IDs
        numeric_pathways, alphanumeric_pathways, common_pathways = check_combinations(target_uniprot_ids, total_ids)
        
        # Convert DataFrames to JSON format
        numeric_json = numeric_pathways.to_json(orient='records')
        alphanumeric_json = alphanumeric_pathways.to_json(orient='records')
        common_json = common_pathways.to_json(orient='records')

        return jsonify({
            "numeric_pathways": numeric_json,
            "alphanumeric_pathways": alphanumeric_json,
            "common_pathways": common_json
        })
    
    except Exception as e:
        return jsonify({"error": str(e)}), 500  # Return error message and HTTP status code 500 if an exception occurs

@app.route('/pathways')
def pathways():
    return render_template('pathways.html')


@app.route('/')
def plotting():
    return render_template('home.html')

@app.route('/help')
def help():
    return render_template('help.html')

@app.route('/about')
def about():
    return render_template('about.html')
if __name__ == '__main__':
    app.run(debug=True)
