from flask import Flask, render_template, request, jsonify, flash, session, send_file, redirect
import pandas as pd
import os
import csv
from werkzeug.utils import secure_filename
import plotly.graph_objects as go
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from scipy.stats import binom
from flask import url_for
import logging
app = Flask(__name__)
app.secret_key = 'your_secret_key'
UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'csv'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# Get the current directory
current_dir = os.path.dirname(os.path.realpath(__file__))
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/index')
def index():
    return render_template('index.html')



# Load CSV data into memory for hits search
csv_file_path_hits = 'static/updated_1-501.csv'
data_hits = pd.read_csv(csv_file_path_hits)

# Load CSV data into memory for compound search
csv_file_path_compound = 'static/target_merged_file_2.csv'
data_compound = pd.read_csv(csv_file_path_compound)

@app.route('/search', methods=['POST'])
def search_data_hits():
    try:
        search_term = request.get_json()['searchTerm'].lower()  # Convert search term to lowercase

        # Convert 'Compound' and 'Compounds SMILES' column values to lowercase for case-insensitive comparison
        data_hits['Compound'] = data_hits['Compound'].str.lower()
        data_hits['Compounds SMILES'] = data_hits['Compounds SMILES'].str.lower()

        # Ensure correct column names for searching from your CSV file (modify if needed)
        filtered_data = data_hits[(data_hits['Compound'] == search_term) | (data_hits['Compounds SMILES'] == search_term)]

        if filtered_data.empty:
            return jsonify({'error': 'No data found for the compound name.'}), 404

        # Sort the filtered data by scores in descending order
        filtered_data = filtered_data.sort_values(by='Score', ascending=False)
        filtered_data = filtered_data.fillna('')
        targets = filtered_data['Target'].tolist()
        scores = filtered_data['Score'].tolist()

        # Create the bar graph using Plotly
        fig = go.Figure(data=[go.Bar(x=targets, y=scores)])
        fig.update_layout(title='Target vs. Score for Compound: ' + search_term,
                          xaxis_title='Target', yaxis_title='Score')
        graph_json = fig.to_json()

        # Prepare data for table
        table_columns = ['Target', 'UniProt ID', 'Score', 
                         'Most Similar Compound', 'Compounds SMILES', 
                         'Compound FP2 Similarity', 'Compound Pharmacophore Similarity', 
                         'Assay Value', 'Assay Type']
        table_data = filtered_data[table_columns].to_dict('records')

        return jsonify({'targets': targets, 'scores': scores, 'graph': graph_json, 'table': table_data})

    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/searchtarget', methods=['POST'])
def search_data_compound():
    try:
        search_term = request.get_json()['searchTerm'].lower()  # Convert search term to lowercase

        # Convert 'Compound' column values to lowercase for case-insensitive comparison
        data_compound['Compound'] = data_compound['Compound'].str.lower()

        # Ensure correct column names for searching from your CSV file (modify if needed)
        filtered_data = data_compound[(data_compound['Compound'] == search_term)]

        if filtered_data.empty:
            return jsonify({'error': 'No data found for the compound name.'}), 404

        # Sort the filtered data by scores in descending order
        filtered_data = filtered_data.sort_values(by='Score', ascending=False)
        filtered_data = filtered_data.fillna('')
        targets = filtered_data['Target'].tolist()
        scores = filtered_data['Score'].tolist()

        # Create the bar graph using Plotly
        fig = go.Figure(data=[go.Bar(x=targets, y=scores)])
        fig.update_layout(title='Compounds vs. Score for Target: ' + search_term,
                          xaxis_title='Compounds', yaxis_title='Score')
        graph_json = fig.to_json()

        # Prepare data for table
        table_columns = ['Target', 'Compound', 'Score']
        table_data = filtered_data[table_columns].to_dict('records')

        return jsonify({'targets': targets, 'scores': scores, 'graph': graph_json, 'table': table_data})

    except Exception as e:
        return jsonify({'error': str(e)}), 500

#plants 
# Load compound information (name, SMILES, taxonomy)
compound_info_csv_file = "static/combined.csv"
compound_data = pd.read_csv(compound_info_csv_file)
print(compound_data)
# plate data tab 
# Load your CSV file containing compound names, SMILES, and taxonomy
plant_csv_file = "static/compound_mapping2.csv"
compound_info= pd.read_csv(plant_csv_file)    



# Configure logging
logging.basicConfig(level=logging.DEBUG)

@app.route('/searchplant', methods=['POST'])
def search_plant_data():
    try:
        search_term = request.get_json()['searchTerm'].lower()  # Convert search term to lowercase

        # Filter compound information based on the provided taxonomy
        filtered_compound_info = compound_info[compound_info['Taxonomy'].str.lower() == search_term]
        if filtered_compound_info.empty:
            return jsonify({'error': 'No compounds found for the provided taxonomy.'}), 404

        # Get the list of compound names for the filtered compounds
        compound_names = filtered_compound_info['Compound Name']

        # Filter compound data based on the list of compound names
        filtered_compound_data = compound_data[compound_names]

        # Extract target names from the first column of compound_data
        target_names = compound_data.iloc[:, 0]

        # Initialize lists to store top target and score for each compound
        top_targets = []
        top_scores = []

        # Iterate through each compound
        for compound in compound_names:
            # Find the target with the maximum score for the current compound
            max_score = filtered_compound_data[compound].max()
            max_target_index = filtered_compound_data[compound].idxmax()
            max_target_name = target_names[max_target_index]

            # Append top target and score to the respective lists
            top_targets.append(max_target_name)
            top_scores.append(max_score)

        # Create a new DataFrame with compound name, SMILES, and the highest scoring target along with its score
        result_table = pd.DataFrame({
            'Compound Name': compound_names,
            'SMILES': filtered_compound_info['SMILES'],
            'Top Target': top_targets,
            'Score': top_scores
        })

        # Convert DataFrame to dictionary
        table_data = result_table.to_dict('records')

        return jsonify({'table': table_data})
    except Exception as e:
        logging.error(f"Error occurred: {str(e)}")
        return jsonify({'error': str(e)}), 500

  #auto complete_funtions


#Formulation Webpage 
csv_file = "static/combined.csv"
data = pd.read_csv(csv_file, index_col=0)  # Assuming index_col=0

# Load your CSV file containing compound names, SMILES, and taxonomy
smiles_csv_file = "static/compound_mapping2.csv"
smiles_data = pd.read_csv(smiles_csv_file)

def calculate_batch_with_column_name(query_value_pairs):
    results = {}
    for query, value in query_value_pairs:
        try:
            value = float(value)
        except ValueError:
            return "Please provide a valid numerical value"

        if query not in data.columns:
            return "Invalid query: {}".format(query)

        targets = dict(data[query] * value)
        for target, score in targets.items():
            if target in results:
                results[target] += score
            else:
                results[target] = score

    # Filter out targets with score 0
    results = {k: v for k, v in results.items() if v != 0}

    # Normalize the scores
    scores = list(results.values())
    scaler = MinMaxScaler()
    normalized_scores = scaler.fit_transform(pd.DataFrame(scores))

    # Update results dictionary with normalized scores
    normalized_results = {target: normalized_score[0] for target, normalized_score in zip(results.keys(), normalized_scores)}

    # Sort the normalized results
    sorted_results = sorted(normalized_results.items(), key=lambda x: x[1], reverse=True)
    return sorted_results

def calculate_batch_with_smiles(smiles_value_pairs):
    results = {}
    for smiles, value in smiles_value_pairs:
        matching_rows = smiles_data[smiles_data['SMILES'] == smiles]
        if matching_rows.empty:
            return "No compounds found for SMILES: {}".format(smiles)

        for _, row in matching_rows.iterrows():
            compound_name = row['Compound Name']
            if compound_name not in data.columns:
                continue  # Skip if compound name not found in data

            try:
                value = float(value)
            except ValueError:
                return "Invalid value for SMILES: {}".format(value)

            targets = dict(data[compound_name] * value)
            for target, score in targets.items():
                if target in results:
                    results[target] += score
                else:
                    results[target] = score

    # Filter out targets with score 0
    results = {k: v for k, v in results.items() if v != 0}

    if results:
        # Normalize the scores if there are results
        scores = list(results.values())
        scaler = MinMaxScaler()
        normalized_scores = scaler.fit_transform(pd.DataFrame(scores))

        # Update results dictionary with normalized scores
        normalized_results = {target: normalized_score[0] for target, normalized_score in zip(results.keys(), normalized_scores)}

        # Sort the normalized results
        sorted_results = sorted(normalized_results.items(), key=lambda x: x[1], reverse=True)
        return sorted_results
    else:
        return "No results found for given SMILES"


# Function to calculate using taxonomy name for batch
def calculate_batch_with_taxonomy(taxonomy_value_pairs):
    results = {}
    for taxonomy, value in taxonomy_value_pairs:
        matching_rows = smiles_data[smiles_data['Taxonomy'] == taxonomy]
        if matching_rows.empty:
            return "No compounds found for taxonomy: {}".format(taxonomy)

        for _, row in matching_rows.iterrows():
            compound_name = row['Compound Name']
            if compound_name not in data.columns:
                continue  # Skip if compound name not found in data
            targets = dict(data[compound_name] * float(value))
            for target, score in targets.items():
                if target in results:
                    results[target] += score
                else:
                    results[target] = score

    # Filter out targets with score 0
    results = {k: v for k, v in results.items() if v != 0}

    # Normalize the scores
    scores = list(results.values())
    scaler = MinMaxScaler()
    normalized_scores = scaler.fit_transform(pd.DataFrame(scores))

    # Update results dictionary with normalized scores
    normalized_results = {target: normalized_score[0] for target, normalized_score in zip(results.keys(), normalized_scores)}

    # Sort the normalized results
    sorted_results = sorted(normalized_results.items(), key=lambda x: x[1], reverse=True)
    return sorted_results

# @app.route('/search-formulations', methods=['POST'])
# def calculate_batch():
#     try:
#         batch_data = request.form['batch'] 

#         if not batch_data.strip():
#             return "Please provide batch data", 400

#         # Parse batch data to extract compound names/SMILES/taxonomy and values
#         lines = batch_data.strip().split('\n')
#         query_value_pairs = []
#         for line in lines:
#             parts = line.strip().split('\t')
#             if len(parts) == 2:
#                 query = parts[0].strip()
#                 value = parts[1].strip()
#                 query_value_pairs.append((query, value))

#         smiles_queries = [query for query, _ in query_value_pairs if query in smiles_data['SMILES'].values]
#         taxonomy_queries = [query for query, _ in query_value_pairs if query in smiles_data['Taxonomy'].values]
#         print(taxonomy_queries)
#         compound_name_queries = [query for query, _ in query_value_pairs if query in data.columns]
#         print(compound_name_queries)
#         if len(smiles_queries) + len(taxonomy_queries) + len(compound_name_queries) != len(query_value_pairs):
#             return "Invalid queries. Please provide valid SMILES, taxonomy names, or compound names."

#         results = {}
#         if smiles_queries:
#             results.update(calculate_batch_with_smiles([(query, value) for query, value in query_value_pairs if query in smiles_queries]))
#         if taxonomy_queries:
#             results.update(calculate_batch_with_taxonomy([(query, value) for query, value in query_value_pairs if query in taxonomy_queries]))
#         if compound_name_queries:
#             results.update(calculate_batch_with_column_name([(query, value) for query, value in query_value_pairs if query in compound_name_queries]))

#         sorted_results = sorted(results.items(), key=lambda x: x[1], reverse=True)
#         print(sorted_results)

#         total_value = sum(value for _, value in sorted_results)
#         normalized_results = [(key, value / total_value) for key, value in sorted_results]

#         return jsonify(sorted_results)


#     except KeyError:
#         return "Error: Missing 'batch' key in form data", 400

uniprot_id_mapping_file = os.path.join(current_dir, 'static', 'uniport_id_mapping.csv')
@app.route('/search-formulations', methods=['POST'])
def calculate_batch():
    try:
        batch_data = request.form['batch'] 

        if not batch_data.strip():
            return "Please provide batch data", 400

        # Parse batch data to extract compound names/SMILES/taxonomy and values
        lines = batch_data.strip().split('\n')
        query_value_pairs = []
        all_predictions = {}  # Initialize the all_predictions dictionary
        for line in lines:
            parts = line.strip().split('\t')
            if len(parts) == 2:
                query = parts[0].strip()
                value = parts[1].strip()
                query_value_pairs.append((query, value))
                
                # Populate the all_predictions dictionary
                if query in data.columns:
                    targets = [query]
                    scores = [float(value)]
                    all_predictions[query] = {'Targets': targets, 'Scores': pd.Series(scores)}


        smiles_queries = [query for query, _ in query_value_pairs if query in smiles_data['SMILES'].values]
        taxonomy_queries = [query for query, _ in query_value_pairs if query in smiles_data['Taxonomy'].values]
        compound_name_queries = [query for query, _ in query_value_pairs if query in data.columns]
        if len(smiles_queries) + len(taxonomy_queries) + len(compound_name_queries) != len(query_value_pairs):
            return "Invalid queries. Please provide valid SMILES, taxonomy names, or compound names."

        results = {}
        if smiles_queries:
            results.update(calculate_batch_with_smiles([(query, value) for query, value in query_value_pairs if query in smiles_queries]))
        if taxonomy_queries:
            results.update(calculate_batch_with_taxonomy([(query, value) for query, value in query_value_pairs if query in taxonomy_queries]))
        if compound_name_queries:
            results.update(calculate_batch_with_column_name([(query, value) for query, value in query_value_pairs if query in compound_name_queries]))

        sorted_results = sorted(results.items(), key=lambda x: x[1], reverse=True)
        # print(sorted_results)

        total_value = sum(value for _, value in sorted_results)
        normalized_results = [(key, value / total_value) for key, value in sorted_results]
        print(normalized_results)
        # Pass the normalized_results to the second code
        uniprot_id_mapping = pd.read_csv(uniprot_id_mapping_file, index_col='Target Name')['UniProt ID'].to_dict()
        all_pathway_results = {}

        for formulation, score in normalized_results:
            # Check if the formulation is a string
            if isinstance(formulation, str):
                # Assuming the formulation is a compound name
                targets = [formulation]
                scores = [score]

                # Initialize the list to store UniProt IDs with scores
                uniprot_ids_with_scores = []

                # Iterate over targets and scores simultaneously
                for target, score in zip(targets, scores):
                    # Check if the target has a corresponding UniProt ID in the mapping
                    uniport_id = uniprot_id_mapping.get(target, '')
                    if uniport_id:
                        print(uniport_id)
                        # Append the UniProt ID and score to the list
                        uniprot_ids_with_scores.append((uniport_id, score))

                # Update the pathway results dictionary with UniProt IDs and scores
                all_pathway_results[formulation] = uniprot_ids_with_scores
                # print(all_pathway_results[formulation])
            else:
                print(f"Ignoring entry with invalid format: {formulation}")

        # Retrieve UniProt IDs and their scores from all pathway results
        user_genes = set()
        uniport_weights = {}
        for formulation, uniprot_ids_with_scores in all_pathway_results.items():
            for entry in uniprot_ids_with_scores:
                # Check if the entry has exactly two elements
                if len(entry) == 2:
                    uniprot_id, score = entry
                    user_genes.add(uniprot_id)
                    if uniprot_id not in uniport_weights:
                        uniport_weights[uniprot_id] = score
                    else:
                        uniport_weights[uniprot_id] += score
                else:
                    print(f"Ignoring entry with invalid format: {entry}")

        # Function to calculate binomial p-value
        def calculate_pvalue(pathway_genes, all_genes, user_genes):
            M = len(all_genes)  # Total number of genes considered
            K = len(user_genes)  # Number of user-provided genes
            x = len(set(pathway_genes) & user_genes)  # Number of user genes in the pathway
            
            # Calculate the binomial p-value
            pvalue = 1 - binom.cdf(x - 1, K, len(pathway_genes) / M)
                # If p-value is too small, set it to a minimum threshold
            min_threshold = 1.11e-16
            if pvalue < min_threshold:
                pvalue = min_threshold
            # Convert p-value to scientific notation with 2 digits after the decimal point
            pvalue = "{:.2e}".format(pvalue)
            return pvalue

        # Load the CSV file
        df = pd.read_csv('static/UniProt2Reactome_All_Levels.txt', sep='\t', header=None, names=['UniProt ID', 'Pathway ID', 'Pathway URL', 'Pathway Name', 'Evidence Code', 'Species'])

        # Filter for Homo sapiens species
        df = df[df['Species'] == 'Homo sapiens']
        # Get the list of all UniProt IDs
        all_genes = set(df['UniProt ID'].unique())

        # Create a dictionary to store pathway information
        pathway_info = {}

        # Iterate over each pathway
        for pathway_id, pathway_name in zip(df['Pathway ID'].unique(), df['Pathway Name'].unique()):
            pathway_genes = set(df.loc[df['Pathway ID'] == pathway_id, 'UniProt ID'])
            mapped_entities = len(pathway_genes & user_genes)
            total_entities = len(pathway_genes)
            
            # Calculate pathway score based on mapped entities and UniProt ID weights
            score = sum(uniport_weights.get(gene, 0) for gene in pathway_genes) * mapped_entities / len(user_genes) if len(user_genes) > 0 else 0
            
            # Check if the intersection between pathway genes and user genes is non-empty
            if mapped_entities > 0:
                pvalue = calculate_pvalue(pathway_genes, all_genes, user_genes)
                pathway_info[pathway_id] = {'Pathway Name': pathway_name, 'p-value': pvalue, 'Mapped Entities': mapped_entities, 'Total Entities': total_entities, 'Score': score}

        # Normalize pathway scores using min-max normalization to range between 0.1 and 0.9
        min_score = min(info['Score'] for info in pathway_info.values())
        max_score = max(info['Score'] for info in pathway_info.values())
        range_min = 0.1
        range_max = 0.9

        for info in pathway_info.values():
            normalized_score = ((info['Score'] - min_score) / (max_score - min_score)) * (range_max - range_min) + range_min if max_score != min_score else 0.5
            info['Normalized Score'] = round(normalized_score, 2)

        # Sort the pathways based on normalized score
        sorted_pathways = sorted(pathway_info.items(), key=lambda x: x[1]['Normalized Score'], reverse=True)

        pathway_results = []
        for pathway_id, info in sorted_pathways:
                    pathway_name = info['Pathway Name']
                    mapped_entities = info['Mapped Entities']
                    total_entities = info['Total Entities']
                    p_value = info['p-value']
                    # fdr = info['fdr']
                    normalized_score = info['Normalized Score']

                    # Construct the pathway diagram filename
                    diagram_filename = f"{pathway_id}.png"
                    diagram_path = os.path.join('/home/srinivasan/Desktop/CANDI/static/diagram/', diagram_filename)

                    # Check if the diagram file exists
                    if os.path.exists(diagram_path):
                        # Provide link to saved pathway diagram along with pathway name in the JSON response
                        pathway_info = {
                            'pathway_id': pathway_id,
                            'pathway_name': pathway_name,
                            # 'diagram_url': url_for('serve_pathway_diagram', filename=diagram_filename),
                            'mapped_entities': mapped_entities,
                            'total_entities': total_entities,
                            'p_value': p_value,
                            # 'fdr': fdr,
                            'normalized_score': normalized_score
                        }
                        pathway_results.append(pathway_info)

        all_pathway_results[formulation] = pathway_results

        #     # Save predictions to a single CSV file
        # predictions_file_path = os.path.join(current_dir, 'static', 'predicted_targets.csv')
        # save_predictions_to_csv(all_predictions, predictions_file_path)
        # # Call the function to save the pathway results
        # pathway_results_file_path = os.path.join(current_dir, 'static', 'pathway_results.csv')
        # save_pathway_results_to_csv(all_pathway_results, pathway_results_file_path)

        return render_template('results4_layout2.html', normalized_results=sorted_results, pathway_results=pathway_results, uniprot_id_mapping=uniprot_id_mapping)
        # return render_template('results4_layout2.html', predictions=all_predictions, pathway_results=all_pathway_results, uniprot_id_mapping=uniprot_id_mapping)
    # return render_template('index5.html')

    #     return jsonify(sorted_pathways)

    except KeyError:
        return render_template('compounds_2.html', error_message="Error: Missing 'batch' key in form data."), 400


@app.route('/target')
def target():
    return render_template('compounds_2.html')

@app.route('/pathways')
def pathways():
    return render_template('pathways.html')


@app.route('/')
def plotting():
    return render_template('home.html')

@app.route('/help')
def help():
    return render_template('help.html')

@app.route('/about')
def about():
    return render_template('about.html')
if __name__ == '__main__':
    app.run(debug=True)
