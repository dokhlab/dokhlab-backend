function searchCompoundData() {
    const searchTerm = document.getElementById('compound_search_term').value;

    fetch('search', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ searchTerm })
    })
    .then(response => response.json())
    .then(data => {
        if (data.error) {
            alert(data.error);
            return;
        }

        const graphJSON = data.graph;
        const tableData = data.table;  // Data for table

        if (!tableData || tableData.length === 0) {
            alert('No data found for the compound name.');
            return;
        }

        // Create the bar graph using Plotly (if graphJSON is available)
        if (graphJSON) {
            const graph = JSON.parse(graphJSON);
            Plotly.newPlot('compound_bar_graph', graph.data, graph.layout);
        }

        // Render table
        renderCompoundTable(tableData);
    });
}

// Function to render compound data table
function renderCompoundTable(tableData) {
    // Create the DataTable
    const table = $('#compound_data_table').DataTable({
        data: tableData,
        columns: Object.keys(tableData[0]).map(key => ({ title: key, data: key })),
        responsive: true,
        scrollX: true,
        paging: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pageLength: 10,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    // Show the download CSV button
    const downloadBtn = document.getElementById('downloadCSVBtn');
    downloadBtn.style.display = 'inline-block';

    downloadBtn.addEventListener('click', () => {
        const csvContent = convertToCSV(tableData);
        downloadCSV(csvContent);
    });

    // Adjust the graph layout based on the table size
    const graphContainer = document.getElementById('compound_bar_graph');
    const tableWidth = $('#compound_data_table').width();
    const tableHeight = $('#compound_data_table').height();

    Plotly.relayout('compound_bar_graph', {
        width: tableWidth,
        height: tableHeight * 0.6 // Adjust the height to 60% of the table height
    });
}

function adjustGraphSize() {
    const graphContainer = document.getElementById('compound_bar_graph');
    const tableContainer = document.getElementById('compound_data_table_wrapper');

    if (graphContainer && tableContainer) {
        const tableWidth = tableContainer.offsetWidth;
        const tableHeight = tableContainer.offsetHeight;

        Plotly.relayout('compound_bar_graph', {
            width: tableWidth,
            height: tableHeight * 0.6 // Adjust the height to 60% of the table height
        });
    }
}


function resetCompoundSearch() {
    // Clear the search input
    document.getElementById('compound_search_term').value = '';

    // Clear the table and destroy the DataTable instance
    const table = $('#compound_data_table').DataTable();
    table.clear().destroy();

    // Clear the bar graph
    document.getElementById('compound_bar_graph').innerHTML = '';
    Plotly.purge('compound_bar_graph'); // Clear the graph completely

    // Remove the download button
    $('#compound_data_table_wrapper button').remove();
    
    // Hide the download CSV button
    const downloadBtn = document.getElementById('downloadCSVBtn');
    downloadBtn.style.display = 'none';

    // Clear the table headers
    $('#compound_data_table thead').empty();

    // Hide any error messages
    document.getElementById('error_message').innerText = '';
}


// Attach event listener for reset button for compound search
document.getElementById('resetCompoundSearchBtn').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent default form submission
    resetCompoundSearch(); // Call the reset function
});

// Call searchCompoundData function when search button is clicked
document.getElementById('searchCompoundBtn').addEventListener('click', searchCompoundData);

// Function to convert table data to CSV format
function convertToCSV(tableData) {
    const header = Object.keys(tableData[0]).join(',');
    const rows = tableData.map(row => Object.values(row).join(',')).join('\n');
    return `${header}\n${rows}`;
}

// Function to download CSV file
function downloadCSV(csvContent) {
    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    const link = document.createElement('a');
    const url = URL.createObjectURL(blob);
    link.setAttribute('href', url);
    link.setAttribute('download', 'compound_data.csv');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
