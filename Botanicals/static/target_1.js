function searchTargetData() {
    const searchTerm = document.getElementById('target_search_term').value;

    fetch('searchtarget', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ searchTerm })
    })
    .then(response => response.json())
    .then(data => {
        if (data.error) {
            alert(data.error);
            return;
        }

        const targets = data.targets;
        const scores = data.scores;
        const graphJSON = data.graph;
        const tableData = data.table;  // Data for table

        if (!targets || !scores) {
            alert('No data found for the target name.');
            return;
        }

        if (graphJSON) {
            const graph = JSON.parse(graphJSON);
            graph.layout.plot_bgcolor = '#76A849'; // Set the plot background color
            graph.layout.barmode = 'group'; // Set the bar mode to 'group' for grouped bars
            graph.layout.bargap = 0.2; // Set the gap between bars
            graph.layout.xaxis = {
                title: 'Compounds'
            }; // Set X-axis title
            graph.layout.yaxis = {
                title: 'Score'
            }; // Set Y-axis title
            graph.data.forEach((trace) => {
                trace.marker = {
                    color: '#7B49A8' // Set the color for each bar
                };
            });
            Plotly.newPlot('target_bar_graph', graph.data, graph.layout);
        }

        // Render table as a DataTable
        renderDataTable(tableData);
    });
}

function renderDataTable(tableData) {
    // Create the DataTable
    const table = $('#target_data_table').DataTable({
        data: tableData,
        columns: Object.keys(tableData[0]).map(key => ({ title: key, data: key })),
        responsive: true,
        scrollX: true,
        paging: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pageLength: 10,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    // Show the download CSV button
    const downloadBtn = document.getElementById('downloadTargetBtn');
    downloadBtn.style.display = 'inline-block';

    downloadBtn.addEventListener('click', () => {
        const csvContent = convertToCSV(tableData);
        downloadCSV(csvContent);
    });

    // Adjust the graph layout based on the table size
    const graphContainer = document.getElementById('target_bar_graph');
    const tableWidth = $('#target_data_table').width();
    const tableHeight = $('#target_data_table').height();

    Plotly.relayout('target_bar_graph', {
        width: tableWidth,
        height: tableHeight * 0.6 // Adjust the height to 60% of the table height
    });
}

function adjustGraphSize() {
    const graphContainer = document.getElementById('target_bar_graph');
    const tableContainer = document.getElementById('target_data_table');

    if (graphContainer && tableContainer) {
        const tableWidth = tableContainer.offsetWidth;
        const tableHeight = tableContainer.offsetHeight;

        Plotly.relayout('target_bar_graph', {
            width: tableWidth,
            height: tableHeight * 0.6 // Adjust the height to 60% of the table height
        });
    }
}

function resetTargetData() {
    // Clear the search input
    document.getElementById('target_search_term').value = '';

    // Clear the table and destroy the DataTable instance
    const table = $('#target_data_table').DataTable();
    table.destroy();

    // Clear the table container
    const targetDataTable = document.getElementById('target_data_table');
    targetDataTable.innerHTML = '';
    

    // Clear the bar graph
    document.getElementById('target_bar_graph').innerHTML = '';

    // Hide the download CSV button
    const downloadBtn = document.getElementById('downloadTargetBtn');
    downloadBtn.style.display = 'none';

    // Hide any error messages
    document.getElementById('error_message').innerText = '';
}

// Attach event listener for reset button for target search
document.getElementById('resetTargetSearchBtn').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent default form submission
    resetTargetData(); // Call the reset function
});

// Function to convert table data to CSV format
function convertToCSV(tableData) {
    const header = Object.keys(tableData[0]).join(',');
    const rows = tableData.map(row => Object.values(row).join(',')).join('\n');
    return `${header}\n${rows}`;
}

// Function to download CSV file
function downloadCSV(csvContent) {
    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    const link = document.createElement("a");
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "target_data.csv");
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
