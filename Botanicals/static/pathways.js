document.getElementById("batchForm").addEventListener("submit", function(event) {
    event.preventDefault();
    calculateBatch();
});

var dataTable = null; // Declare a variable to hold the DataTable instance

function calculateBatch() {
    var batchData = document.getElementById("batchInput").value;

    fetch("/search-pathways", {
        method: "POST",
        body: new URLSearchParams({
            batch: batchData
        }),
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    })
    .then(response => response.json())
    .then(data => {
        displayResults(data);
        document.getElementById("results").style.display = "block"; // Display the results section
        document.getElementById("downloadBtn").style.display = "block"; // Display the download button
        
        if (dataTable) {
            dataTable.destroy(); // Destroy the existing DataTable instance
        }
    })
    .catch(error => {
        console.error('Error:', error);
        // Clear previous results and display an error message to the user
        clearFormulationResults();
        document.getElementById('error-message').innerText = 'An error occurred while processing the request. Please try again later.';
    });
}

function displayResults(results) {
    var resultsTableBody = document.getElementById("resultsTableBody");
    resultsTableBody.innerHTML = ""; // Clear previous table

    // Check if the results object has the pathways_data property
    if (results.hasOwnProperty('pathways_data')) {
        // Parse the JSON string in pathways_data to convert it into an array of objects
        var pathwaysData = JSON.parse(results.pathways_data);
        
        // Iterate over each pathway object in pathwaysData
        pathwaysData.forEach(function(pathway) {
            var tableRow = document.createElement("tr");
            var pathwayIDCell = document.createElement("td");
            var pathwayNameCell = document.createElement("td");
            var pValueCell = document.createElement("td");
            var totalEntitiesCell = document.createElement("td");
            var mappedIDsCell = document.createElement("td");
            var userIDsCell = document.createElement("td");
            var scoreCell = document.createElement("td");

            pathwayIDCell.textContent = pathway["Pathway identifier"];
            pathwayNameCell.textContent = pathway["Pathway name"];
            pValueCell.textContent = pathway["Entities pValue"];
            totalEntitiesCell.textContent = pathway["Total entities"];
            mappedIDsCell.textContent = pathway["Mapped IDs"];
            userIDsCell.textContent = pathway["User IDs"];
            scoreCell.textContent = pathway["Score"];

            tableRow.appendChild(pathwayIDCell);
            tableRow.appendChild(pathwayNameCell);
            tableRow.appendChild(pValueCell);
            tableRow.appendChild(totalEntitiesCell);
            tableRow.appendChild(mappedIDsCell);
            tableRow.appendChild(userIDsCell);
            tableRow.appendChild(scoreCell);

            resultsTableBody.appendChild(tableRow);
        });
    } else {
        console.error('Results object does not contain the pathways_data property:', results);
    }
}



document.getElementById("downloadBtn").addEventListener("click", function() {
    var csvContent = "data:text/csv;charset=utf-8,";
    csvContent += "Pathway Identifier,Pathway Name,Entities pValue,Total Entities,Mapped IDs,User IDs,Score\n";

    var results = document.querySelectorAll("#resultsTableBody tr"); // Corrected query selector
    results.forEach(function(result) {
        var cells = result.cells;
        var rowData = [];
        for (var i = 0; i < cells.length; i++) {
            rowData.push(cells[i].textContent);
        }
        csvContent += rowData.join(",") + "\n";
    });

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "pathways_results.csv");
    document.body.appendChild(link); // Required for Firefox
    link.click();
});

// Function to reset formulation search
function resetFormulationSearch() {
    // Clear the batch input
    document.getElementById('batchInput').value = '';

    // Clear previous results
    clearFormulationResults();

    // Hide the results and download button
    document.getElementById('results').style.display = 'none';
    document.getElementById('downloadBtn').style.display = 'none';
}

// Attach event listener for reset button
document.getElementById('resetpathwayBtn').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent default form submission
    resetFormulationSearch(); // Call the reset function
});

function clearFormulationResults() {
    var resultsTableBody = document.getElementById('resultsTableBody');
    resultsTableBody.innerHTML = ''; // Clear results table body

    // Clear error message if it exists
    var errorMessage = document.getElementById('error-message');
    if (errorMessage) {
        errorMessage.innerText = '';
    }
}
