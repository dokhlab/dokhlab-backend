// Function to reset plant search
function resetPlantSearch() {
    // Clear the search input
    document.getElementById('plant_search_term').value = '';

    // Hide the download button
    document.getElementById('downloadPlantBtn').style.display = 'none';

    // Clear the table
    const table = $('#plant_data_table').DataTable();
    table.destroy();

    // Clear the table container
    const plantDataTable = document.getElementById('plant_data_table');
    plantDataTable.innerHTML = '';
}

// Function to search plant data
function searchPlantData() {
    const searchTerm = document.getElementById('plant_search_term').value;

    // Send a POST request to the server with the search term
    fetch('searchplant', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            searchTerm: searchTerm
        })
    })
    .then(response => response.json())
    .then(data => {
        if (data.error) {
            // Handle error
            console.error(data.error);
        } else {
            // Populate the table with search results
            populatePlantTable(data.table);

            // Show the download button
            document.getElementById('downloadPlantBtn').style.display = 'inline-block';

            // Store table data in a global variable for download
            window.plantTableData = data.table;

            // Attach event listener for download button
            document.getElementById("downloadPlantBtn").onclick = function() {
                downloadPlantData(window.plantTableData);
            };
        }
    })
    .catch(error => {
        console.error('Error:', error);
    });
}

// Function to populate plant data table
function populatePlantTable(tableData) {
    // Create the DataTable
    const table = $('#plant_data_table').DataTable({
        data: tableData,
        columns: Object.keys(tableData[0]).map(key => ({ title: key, data: key })),
        responsive: true,
        scrollX: true,
        paging: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pageLength: 10,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}

// Function to download table data as CSV
function downloadPlantData(tableData) {
    const csvContent = "data:text/csv;charset=utf-8," + tableData.map(row =>
        Object.values(row).join(",")
    ).join("\n");

    const encodedUri = encodeURI(csvContent);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "plant_data.csv");
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

// Call searchPlantData function when search button is clicked
document.getElementById('search_button').addEventListener('click', searchPlantData);

// Attach event listener for reset button
document.getElementById('resetPlantBtn').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent default form submission
    resetPlantSearch(); // Call the reset function
});
