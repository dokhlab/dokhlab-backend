document.getElementById("batchForm").addEventListener("submit", function(event) {
    event.preventDefault();
    calculateBatch();
});

function calculateBatch() {
    var batchData = document.getElementById("batchInput").value;

    fetch("/search-formulations", {
        method: "POST",
        body: new URLSearchParams({
            batch: batchData
        }),
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    })
    .then(response => response.json())
    .then(data => {
        displayResults(data);
        displayBarPlot(data);
        document.getElementById("results").style.display = "block"; // Display the results section
        document.getElementById("downloadBtn").style.display = "block"; // Display the download button
        // Render table using DataTables
        $('#data_table').DataTable({
            data: data.table,
            columns: [
                { title: 'Target', data: 'Target' },
                { title: 'Score', data: 'Score' },
            ],
            // Enable pagination with 10 rows per page
            paging: true,
            pageLength: 10,
            // Enable searching/filtering
            searching: true,
            // Enable sorting
            ordering: true
        });
    })
    .catch(error => {
        console.error('Error:', error);
    });
}

function displayResults(results) {
    var resultsTableBody = document.getElementById("resultsTableBody");
    resultsTableBody.innerHTML = ""; // Clear previous table

    results.forEach(function(item) {
        var tableRow = document.createElement("tr");
        var targetNameCell = document.createElement("td");
        var scoreCell = document.createElement("td");
        targetNameCell.textContent = item[0];
        scoreCell.textContent = item[1];
        tableRow.appendChild(targetNameCell);
        tableRow.appendChild(scoreCell);
        resultsTableBody.appendChild(tableRow);
    });
}

function displayBarPlot(results) {
    var targetNames = results.slice(0, 10).map(item => item[0]).reverse();
    var scores = results.slice(0, 10).map(item => item[1]).reverse();
  
    var data = [{
      x: scores,
      y: targetNames,
      type: 'bar',
      orientation: 'h'
    }];
  
    // Updated layout options for centering and margin adjustments
    var layout = {
      title: 'Top 10 Target Names and Scores',
      xaxis: { title: 'Score', anchor: 'center' }, // Center X-axis title
      yaxis: { title: 'Target Name' },
      width: 1800,
      margin: {
        l: 750,  // Adjust left margin to avoid cutting off bars
        r: 200,   // Adjust right margin for better spacing
        t: 50,   // Adjust top margin for title placement
        b: 50    // Adjust bottom margin for better spacing
      }
    };
  
    Plotly.newPlot('barPlot', data, layout);
  }

document.getElementById("downloadBtn").addEventListener("click", function() {
    var csvContent = "data:text/csv;charset=utf-8,";
    csvContent += "Target Name,Score\n";

    var results = document.querySelectorAll("#resultsTableBody tr"); // Corrected query selector
    results.forEach(function(result) {
        var targetName = result.cells[0].textContent;
        var score = result.cells[1].textContent;
        csvContent += targetName + "," + score + "\n";
    });

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "results.csv");
    document.body.appendChild(link); // Required for Firefox
    link.click();
});
