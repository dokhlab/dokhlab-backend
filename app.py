import flask
from flask import Flask, abort, redirect, url_for, request, session, send_from_directory, jsonify
from flask_cors import CORS, cross_origin
import mysql.connector
import os
import hashlib
from werkzeug.utils import secure_filename
import json, mimetypes
import random
from flask import send_file
from ftplib import FTP
from ast import literal_eval
from io import BytesIO, StringIO
from subprocess import check_output
import requests, math, datetime
from contextlib import contextmanager
import json
from flask_mail import Mail, Message

from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem.Draw import rdMolDraw2D
from rdkit.Chem import AllChem
import sys, decimal

mail_settings = {
  "MAIL_SERVER": 'smtp-mail.outlook.com',
  "MAIL_PORT": 587,
  "MAIL_USE_TLS": True,
  "MAIL_USE_SSL": False,
  "MAIL_USERNAME": 'dokhlab@outlook.com',
  "MAIL_PASSWORD": 'wait4me2019'
#  "MAIL_USERNAME": os.environ['EMAIL_USER'],
#  "MAIL_PASSWORD": os.environ['EMAIL_PASSWORD']
}

UPLOAD_FOLDER = '/tmp'

app = Flask(__name__)


app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app, support_credentials=True)

app.config.update(mail_settings)
mail = Mail(app)

db_host = os.environ['DBHOST']
db_user = os.environ['DBUSER']
db_pass = os.environ['DBPASS']
db_port = os.environ['DBPORT']
db_dokh = os.environ['DBDOKH']

ftp_user = os.environ['FTP_USER']
ftp_pass = os.environ['FTP_PASSWORD']

server_table = {
  'cpi': 'medusadock_tasks',
  'ifoldrna': 'ifoldrna_tasks',
  'ohm': 'ohm_tasks',
  'drift': 'drift_tasks'
}

def json_default(obj):
  if isinstance(obj, decimal.Decimal):
    return float(obj)
  else:
    return obj

def file_get_contents(filename):
  with open(filename) as f:
    return f.read()

def file_put_contents(filename, content):
  f = open(filename, 'w+')
  f.write(content)
  f.close()

def smi2svg(smi):
  m = Chem.MolFromSmiles(smi)
  AllChem.Compute2DCoords(m)
  d = rdMolDraw2D.MolDraw2DSVG(500, 500)
  d.DrawMolecule(m)
  #rdMolDraw2D.PrepareAndDrawMolecule(d, m)
  d.FinishDrawing()
  s = d.GetDrawingText()
  return s

def wget(url):
  try:
    res = requests.get(url)
    return res.text
  except:
    return ''

@contextmanager
def open_db(dbname=db_dokh, dictionary=True):
  try:
    db = mysql.connector.connect(host=db_host, user=db_user, password=db_pass, database=dbname)
    db.autocommit = True
    cursor = db.cursor(dictionary=dictionary)
    yield cursor
  finally:
    db.close()

@contextmanager
def open_ftp():
  try:
    ftp = FTP(host='ftp', user=ftp_user, passwd=ftp_pass)
    yield ftp
  finally:
    ftp.quit()

def ftp_fetch(path, filename):
  r = BytesIO()
  with open_ftp() as ftp:
    ftp.cwd(path)
    files = ftp.nlst()
    download_name = '{0}'.format(filename)
    ftp.retrbinary('RETR {0}'.format(download_name), r.write)
  r.seek(0)
  return r

def ftp_delete(path, filename):
  with open_ftp() as ftp:
    ftp.cwd(path)
    ftp.delete('{0}'.format(filename))

@app.before_request
def make_session_permanent():
  session.permanent = True
  app.permanent_session_lifetime = datetime.timedelta(minutes=5)

@app.route('/')
def index():
  with open_db() as dokhlab_cursor:
    dokhlab_cursor.execute('select id,email from users')
    r = dokhlab_cursor.fetchall()
  return str(r)

#@app.route('/actions')
#def index():
#  if 'username' in session:
#    return f'Logged in as {session["username"]}'
#  return 'You are not logged in'

@app.route('/login', methods=['POST'])
@cross_origin(supports_credentials=True)
def login():
  if request.method == 'POST':
    username = request.form['username']
    password = request.form['password']
  elif request.method == 'GET':
    username = request.args.get('username', '')
    password = request.args.get('password', '')
  with open_db() as cursor:
    cursor.execute("select * from users where username=%s", (username,))
    r = cursor.fetchall()
  if len(r) == 0:
    return {'status':0}
  password = hashlib.md5(password.encode('utf8')).hexdigest()
#  id0,password0,email0 = r[0]
  id0 = r[0]['id']
  password0 = r[0]['password']
  email0 = r[0]['email']
  if password0 == password:
    session['user'] = r[0]
    return {'status':1,'user':session['user']}
  else:
    return {'status':0}

@app.route('/sign_up', methods=['POST'])
@cross_origin(supports_credentials=True)
def sign_up():
#  return {'status':0, 'msg':'Signup failed!'}
  if request.method == 'POST':
    username = request.form['username']
    password1 = request.form['password1']
    password2 = request.form['password2']
    email = ''
    approved = 0
    firstname = ''
    lastname = ''
    affiliation = ''

    if 'email' in request.form:
      email = request.form['email']
    if 'firstname' in request.form:
      firstname = request.form['firstname']
    if 'lastname' in request.form:
      lastname = request.form['lastname']
    if 'affiliation' in request.form:
      affiliation = request.form['affiliation']

    if password1 != password2:
      return {'status':0, 'msg':'Passwords are not same!'}

    password = hashlib.md5(password1.encode('utf8')).hexdigest()

    with open_db() as dokhlab_cursor:
      dokhlab_cursor.execute("select * from users where username=%s", (username,))
      r = dokhlab_cursor.fetchall()
      if len(r) > 0:
        return {'status':0, 'msg':'Username already exists!'}

      dokhlab_cursor.execute("insert into users(username, password, email, approved, affiliation, firstname, lastname) values(%s, %s, %s, %s, %s, %s, %s)", (username,password,email,approved,affiliation,firstname,lastname))
  #    dokhlab.commit()

      dokhlab_cursor.execute("select * from users where username=%s", (username,))
      r = dokhlab_cursor.fetchall()
      if len(r) > 0:
        session['user'] = r[0]
        return {'status':1, 'user':r[0]}
      else:
        return {'status':0, 'msg':'Signup failed!'}

@app.route('/logout')
@cross_origin(supports_credentials=True)
def logout():
  if 'user' in session:
    username = session['user']['username']
    session.pop('user', None)
    return username
  else:
    return 'none'

@app.route('/user', methods=['GET', 'POST'])
@cross_origin(supports_credentials=True)
def autologin():
  if 'user' in session:
    user = session['user']
    return {'status':1, 'user':user}
  else:
    return {'status':0, 'msg':'Please login'}

@app.route('/yuel/submit', methods=['POST'])
@cross_origin(supports_credentials=True)
def yuel_submit():
  if request.method == 'POST':
    task_id = random.randrange(999999999)
    userid = 0
    task_level = 0
    if 'user' in session:
      user = session['user']
      userid = user['id']
      tasklevel = 1
    title = 'anonymous'
    par = request.files['par'].read()

    with open_db() as dokhlab_cursor:
      dokhlab_cursor.execute("insert into yuel_tasks (tsubmit, id, userid, title, par, level) values(CURRENT_TIMESTAMP(), %s, %s, %s, %s, %s)", (task_id, userid, title, par, task_level))

      return str(task_id);

@app.route('/yuel/status', methods=['GET'])
@cross_origin(supports_credentials=True)
def yuel_status():
  task_id = request.args.get('id', '')

  userid = 0
  task_level = 0
  if 'user' in session:
    user = session['user']
    userid = user['id']
    tasklevel = 1

  with open_db() as dokhlab_cursor:
    dokhlab_cursor.execute("select status from yuel_tasks where id=%s", (task_id,))
    r = dokhlab_cursor.fetchall()

    return r[0]['status'];

@app.route('/yuel/fetch', methods=['GET'])
@cross_origin(supports_credentials=True)
def yuel_fetch():
  task_id = request.args.get('id', '')

  userid = 0
  task_level = 0
  if 'user' in session:
    user = session['user']
    userid = user['id']
    tasklevel = 1

  ftp = FTP(host='ftp', user=ftp_user, passwd=ftp_pass)
  ftp.cwd('yuel')

  r = BytesIO()
  ftp.retrbinary('RETR {0}-aff.txt'.format(task_id), r.write)
  aff = r.getvalue().decode("utf-8")

  r = BytesIO()
  ftp.retrbinary('RETR {0}-comp.svg'.format(task_id), r.write)
  comp = r.getvalue().decode("utf-8")

  r = BytesIO()
  ftp.retrbinary('RETR {0}-prot.txt'.format(task_id), r.write)
  prot = r.getvalue().decode("utf-8")

  ftp.quit()

  return {'aff':aff, 'comp':comp, 'prot':prot}

@app.route('/medusadock/file', methods=['GET'])
@cross_origin(supports_credentials=True)
def medusadock_file():
  name = request.args.get('name', '')
  fmt = request.args.get('format', '')

  return send_from_directory(UPLOAD_FOLDER, '{0}.{1}'.format(name, fmt), as_attachment=True)

def shell_exec(cmd):
  stdout = check_output(cmd).decode('utf-8')
  return stdout

def allows_file(filename, exts):
  return '.' in filename and filename.rsplit('.', 1)[1].lower() in exts

@app.route('/medusadock/set_receptor', methods=['POST'])
@cross_origin(supports_credentials=True)
def medusadock_set_receptor():
  task_id = random.randrange(999999999)
  if 'pdbid' in request.form and request.form['pdbid'] != '':
    pdbid = request.form['pdbid']
    cif = wget("http://files.rcsb.org/download/{0}.cif".format(pdbid))
    with open('{0}/{1}.cif'.format(UPLOAD_FOLDER, task_id), 'w+') as f:
      f.write(cif)
    shell_exec('obabel {0}/{1}.cif -O {0}/{1}.pdb'.format(UPLOAD_FOLDER, task_id).split(' '))
  else:
    r = request.files['file']
    filename = secure_filename(r.filename)
    r.save(os.path.join(UPLOAD_FOLDER, filename))
    shell_exec('obabel {0}/{1} -O {0}/{2}.pdb'.format(UPLOAD_FOLDER, filename, task_id).split(' '))
  return str(task_id)

@app.route('/medusadock/set_ligand', methods=['POST'])
@cross_origin(supports_credentials=True)
def medusadock_set_ligand():
  task_id = random.randrange(999999999)
  if 'zincid' in request.form and request.form['zincid'] != '':
    zincid = request.form['zincid']
    sdf = wget("https://zinc15.docking.org/substances/{0}.sdf".format(zincid))
    with open('{0}/{1}.sdf'.format(UPLOAD_FOLDER, task_id), 'w+') as f:
      f.write(sdf)
    shell_exec('obabel {0}/{1}.sdf -O {0}/{1}.mol2'.format(UPLOAD_FOLDER, task_id).split(' '))
  else:
    r = request.files['file']
    filename = secure_filename(r.filename)
    r.save(os.path.join(UPLOAD_FOLDER, filename))
    shell_exec('obabel {0}/{1} -O {0}/{2}.mol2'.format(UPLOAD_FOLDER, filename, task_id).split(' '))
  return str(task_id)

@app.route('/medusadock/task', methods=['GET'])
@cross_origin(supports_credentials=True)
def medusadock_task():
  task_id = request.args.get('id', '')
  item = request.args.get('item', '')

  with open_db() as cursor:
    if item == '':
      cursor.execute('select id, userid, tsubmit, tprocess, tfinish, receptor, ligand, status, log, output from medusadock_tasks where id=%s', (task_id,))
      r = cursor.fetchall()
      return r[0]
    else:
      if item == 'output':
        download_name = 'dock.pdb'.format(task_id)
      elif item == 'log':
        download_name = 'log.txt'.format(task_id)

      r = ftp_fetch(f'medusadock/{task_id}', download_name)
      return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/medusadock/status', methods=['GET'])
@cross_origin(supports_credentials=True)
def medusadock_status():
  task_id = request.args.get('id', '')

  with open_db() as cursor:
    cursor.execute('select status from medusadock_tasks where id=%s', (task_id,))
    r = cursor.fetchall()
    return r[0]['status']

@app.route('/medusadock/submit', methods=['POST'])
@cross_origin(supports_credentials=True)
def medusadock_submit():
  task_id = random.randrange(999999999)
  userid = 0
  if 'user' in session:
    user = session['user']
    userid = user['id']

  inputs = {}
  for k,v in request.form.to_dict(flat=False).items():
    if k == 'receptor':
      inputs[k] = file_get_contents(os.path.join(UPLOAD_FOLDER, v[0]+'.pdb'))
    elif k == 'ligand':
      inputs[k] = file_get_contents(os.path.join(UPLOAD_FOLDER, v[0]+'.mol2'))
    else:
      inputs[k] = v[0]

  inputs['id'] = task_id
  inputs['userid'] = userid

  with open_db() as dokhlab_cursor:
    keys = ','.join(inputs.keys())
    placeholders = ','.join('%s' for k in inputs.keys())
    values = [inputs[k] for k in inputs.keys()]
    dokhlab_cursor.execute(f'insert into medusadock_tasks ({keys}) values({placeholders})', tuple(values))

    return str(task_id);

@app.route('/medusadock/check', methods=['GET'])
@cross_origin(supports_credentials=True)
def medusadock_check():
  page = int(request.args.get('page', 1))
  page_size = 8
  offset = (page-1)*page_size

  with open_db() as cursor:
    cursor.execute("select id, userid, tsubmit, tprocess, tfinish, status from medusadock_tasks order by tsubmit desc limit %s,%s", (offset, page_size))
    tasks = cursor.fetchall()

    cursor.execute("select count(id) as n from medusadock_tasks")
    n = cursor.fetchall()[0]['n']
    npages = int(math.ceil(n/page_size))

    return {'tasks':tasks, 'pages':npages}

@app.route('/front_config', methods=['GET'])
@cross_origin(supports_credentials=True)
def dokhlab_config():
  download_name = 'front-config.json'
  r = ftp_fetch('dokhlab/configs', download_name)
  return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/update_front_config', methods=['POST'])
@cross_origin(supports_credentials=True)
def update_front_config():
  config = request.files['config']
  print(config)

  with open_ftp() as ftp:
    ftp.cwd('dokhlab/configs')
    ftp.storbinary('STOR front-config.json', config.stream)
    return "1"

@app.route('/content', methods=['GET'])
@cross_origin(supports_credentials=True)
def content():
  name = request.args.get('name')
  download_name = '{0}.html'.format(name)
  r = ftp_fetch('dokhlab/contents', download_name)
  return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/members')
@cross_origin(supports_credentials=True)
def members():
  with open_db() as cursor:
    cursor.execute('select * from mems')
    r = cursor.fetchall()
    return jsonify(r)

@app.route('/publications')
@cross_origin(supports_credentials=True)
def publications():
  with open_db() as cursor:
    cursor.execute('select * from pubs')
    r = cursor.fetchall()
    return jsonify(r)

@app.route('/image')
@cross_origin(supports_credentials=True)
def image():
  download_name = request.args.get('name')
  r = ftp_fetch('dokhlab/images', download_name)
  mimetype = mimetypes.guess_type(download_name, False)[0]
  return send_file(r, download_name=download_name, mimetype=mimetype)

@app.route('/member_image')
@cross_origin(supports_credentials=True)
def member_image():
  name = request.args.get('name')
  download_name = '{0}'.format(name)
  r = ftp_fetch('dokhlab/images/members', download_name)
  return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/download')
@cross_origin(supports_credentials=True)
def download():
  name = request.args.get('name')
  download_name = '{0}'.format(name)
  r = ftp_fetch('dokhlab', download_name)
  if download_name.endswith('.pdf'):
    mimetype = 'application/pdf'
    return send_file(r, download_name=download_name, mimetype=mimetype)
  else:
    return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/table')
@cross_origin(supports_credentials=True)
def table():
  name = request.args.get('name')
  with open_db() as cursor:
    cursor.execute('select * from {0}'.format(name))
    r = cursor.fetchall()
#    print(r)
    return jsonify(r)

@app.route('/erase', methods=['POST'])
@cross_origin(supports_credentials=True)
def erase():
  content = json.loads(request.files['file'].read())
  keys = []
  values = []
  for k,v in content.items():
    if k == '__table':
      table = v
    else:
      if v != '':
        keys.append(k)
        values.append(v)
  query = 'delete from {0} where {1}'.format(table, ' and '.join(['{0}=%s'.format(k) for k in keys]))
  with open_db() as cursor:
    cursor.execute(query, values)
  return 'done'

@app.route('/insert', methods=['POST'])
@cross_origin(supports_credentials=True)
def insert():
  content = json.loads(request.files['file'].read())
  keys = []
  values = []
  ret = {}
  for k,v in content.items():
    if k == '__table':
      table = v
    else:
      if k == 'pasword' or k == 'passwd':
        v = hashlib.md5(v.encode('utf8')).hexdigest()
      keys.append(k)
      values.append(v)
      ret[k] = v
  query = 'insert into {0}({1}) values({2})'.format(table, ', '.join(keys), ', '.join(['%s' for v in values]))
  with open_db() as cursor:
    cursor.execute(query, values)
  return ret

@app.route('/update', methods=['POST'])
@cross_origin(supports_credentials=True)
def update():
  content = json.loads(request.files['file'].read())
  keys = []
  values = []
  ret = {}
  for k,v in content.items():
    if k == '__table':
      table = v
    elif k == '__key':
      key = v

  for k,v in content.items():
    if k != '__table' and k != '__key':
      if k == 'pasword' or k == 'passwd':
        v = hashlib.md5(v.encode('utf8')).hexdigest()
      if k == key:
        value = v
      else:
        keys.append(k)
        values.append(v)
        ret[k] = v

  query = 'update {0} set {1} where {2}=%s'.format(table, ', '.join('{0}=%s'.format(k) for k in keys), key)
  values.append(value)
  with open_db() as cursor:
    cursor.execute(query, values)
  return ret

@app.route('/drift/submit', methods=['POST'])
@cross_origin(supports_credentials=True)
def drift_submit():
  if request.method == 'POST':
    task_id = random.randrange(999999999)
    userid = 0
    task_level = 0
    if 'user' in session:
      user = session['user']
      userid = user['id']
      tasklevel = 1
    par = request.files['par'].read()
    title = json.loads(par)['title']

    with open_db() as dokhlab_cursor:
      dokhlab_cursor.execute("insert into drift_tasks (tsubmit, id, userid, title, par, level) values(CURRENT_TIMESTAMP(), %s, %s, %s, %s, %s)", (task_id, userid, title, par, task_level))

      return str(task_id);

@app.route('/drift/check', methods=['GET'])
@cross_origin(supports_credentials=True)
def drift_check():
  page = int(request.args.get('page', 1))
  page_size = 8
  offset = (page-1)*page_size

  with open_db() as cursor:
#    cursor.execute("select id, userid, tsubmit, tprocess, tfinish, status from drift_tasks order by tsubmit desc limit %s,%s", (offset, page_size))
    cursor.execute("select drift_tasks.id, title, users.username, userid, tsubmit, tprocess, tfinish, status, drift_tasks.level, par from drift_tasks left join users on drift_tasks.userid=users.id order by tsubmit desc limit %s,%s", (offset, page_size))
    tasks = cursor.fetchall()

#  $query = "select drift_tasks.id, title, users.username, userid, tsubmit, tprocess, tfinish, status, drift_tasks.level, par from drift_tasks left join users on drift_tasks.userid=users.id order by tsubmit desc LIMIT $page, 10";

    cursor.execute("select count(id) as n from drift_tasks")
    n = cursor.fetchall()[0]['n']
    npages = int(math.ceil(n/page_size))

    return {'tasks':tasks, 'pages':npages}

@app.route('/drift/task', methods=['GET'])
@cross_origin(supports_credentials=True)
def drift_task():
  taskid = request.args.get('taskid')
  icompound = request.args.get('icompound')

  with open_db() as cursor:
    cursor.execute('select dt.par as par, dt.id as id, dt.title as title, dr.result as result from drift_results dr join drift_tasks dt on dr.taskid=dt.id where dr.taskid=%s and dr.icompound=%s', (taskid, icompound))
    r = cursor.fetchall()
    return r[0]

@app.route('/smiles2svg', methods=['GET'])
@cross_origin(supports_credentials=True)
def smiles2svg():
  smiles = request.args.get('smiles', '')
  svg = smi2svg(smiles)
#  print(svg)
#  return flask.Response(flask.Markup(svg), mimetype='image/svg+xml')
#  return flask.Response(svg, mimetype='text/plain')
#  return svg
#  return flask.Response(svg, mimetype='text/plain')
  svg_io = BytesIO()
  svg_io.write(svg.encode('utf-8'))
  svg_io.seek(0)
#  return svg_io
  return send_file(svg_io, mimetype='image/svg+xml')

@app.route('/drift/search_compound', methods=['GET'])
@cross_origin(supports_credentials=True)
def drift_search_compound():
  query = request.args.get('query', '')

  with open_db('chembl_25') as cursor:
    cursor.execute('SELECT md.chembl_id, md.pref_name, GROUP_CONCAT(DISTINCT cr.compound_name SEPARATOR "; ") as compound_name, GROUP_CONCAT(DISTINCT ms.synonyms SEPARATOR "; ") as synonyms, cp.full_mwt, cp.full_molformula, cs.canonical_smiles, cs.molfile FROM (SELECT molregno FROM molecule_synonyms where synonyms COLLATE UTF8_GENERAL_CI like %s UNION SELECT molregno FROM compound_properties where full_molformula COLLATE UTF8_GENERAL_CI like %s UNION SELECT molregno FROM molecule_dictionary where chembl_id = %s UNION SELECT molregno FROM compound_records where compound_name COLLATE UTF8_GENERAL_CI like %s) as result JOIN molecule_dictionary md ON result.molregno = md.molregno LEFT JOIN compound_properties cp ON result.molregno = cp.molregno LEFT JOIN molecule_synonyms ms ON result.molregno = ms.molregno LEFT JOIN compound_records cr ON result.molregno = cr.molregno LEFT JOIN compound_structures cs ON result.molregno = cs.molregno group by md.molregno;', (query, query, query, '%{0}%'.format(query)))

    r = cursor.fetchall()
#    print(r)
#    return jsonify(r)
    return json.dumps(r, default=json_default)

@app.route('/queue', methods=['GET'])
@cross_origin(supports_credentials=True)
def queue():
  server = request.args.get('server')
  page = int(request.args.get('page', 1))
  page_size = 8
  offset = (page-1)*page_size

  table = server_table[server]

  with open_db() as cursor:
    cursor.execute(f"select id, userid, title, UNIX_TIMESTAMP(tsubmit) as tsubmit, UNIX_TIMESTAMP(tprocess) as tprocess, UNIX_TIMESTAMP(tfinish) as tfinish, status from {table} order by tsubmit desc limit %s,%s", (offset, page_size))
    tasks = cursor.fetchall()

    cursor.execute(f"select count(id) as n from {table}")
    n = cursor.fetchall()[0]['n']
    npages = int(math.ceil(n/page_size))

    return {'tasks':tasks, 'pages':npages}

@app.route('/ifoldrna/submit', methods=['POST'])
@cross_origin(supports_credentials=True)
def ifoldrna_submit():
  try:
    task_id = random.randrange(999999999)
    userid = 0
    if 'user' in session:
      user = session['user']
      userid = user['id']
    par = request.files['par'].read().decode("utf-8")
    title = json.loads(par)['title']

    with open_db() as dokhlab_cursor:
      dokhlab_cursor.execute("insert into ifoldrna_tasks (tsubmit, id, userid, title, files) values(CURRENT_TIMESTAMP(), %s, %s, %s, %s)", (task_id, userid, title, json.dumps({'par':par})))
      return {'status':1,'id':task_id}
  except Exception as e:
      return {'status':0, 'msg':str(e)}

@app.route('/ifoldrna/task', methods=['GET'])
@cross_origin(supports_credentials=True)
def ifoldrna_task():
  task_id = request.args.get('id', '')

  with open_db() as cursor:
    cursor.execute('select * from ifoldrna_tasks where id=%s', (task_id,))
    r = cursor.fetchall()
    return r[0]

@app.route('/ifoldrna/cluster')
@cross_origin(supports_credentials=True)
def ifoldrna_cluster():
  taskid = request.args.get('id')
  cluster = request.args.get('cluster', 1)
  download_name = f'cluster.{cluster}.pdb'
  r = ftp_fetch(f'ifoldrna/{taskid}/dmd.sim', download_name)
  return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/ifoldrna/log')
@cross_origin(supports_credentials=True)
def ifoldrna_log():
  taskid = request.args.get('id')
  download_name = 'log.txt'
  r = ftp_fetch(f'ifoldrna/{taskid}', download_name)
  return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/ohm/task', methods=['GET'])
@cross_origin(supports_credentials=True)
def ohm_task():
  taskid = request.args.get('id')

  with open_db() as cursor:
    cursor.execute('select * from ohm_tasks where id=%s', (taskid,))
    r = cursor.fetchall()
    return r[0]

@app.route('/ohm/result', methods=['GET'])
@cross_origin(supports_credentials=True)
def ohm_result():
  taskid = request.args.get('id')
  f = request.args.get('file')
  download_name = request.args.get('as', '')

  filename = ''
  if f == 'corr':
    filename = f'{taskid}.corr'
  elif f == 'clusters':
    filename = f'{taskid}.clusters'
  elif f == 'cluster-pse':
    filename = f'{taskid}-cluster.pse'
  elif f == 'dendrogram-normal':
    filename = f'{taskid}-dendrogram-normal.png'
  elif f == 'paths':
    filename = f'{taskid}.paths'
  elif f == 'path-pse':
    filename = f'{taskid}-path.pse'
  elif f == 'aci':
    filename = f'{taskid}.nodes'
  elif f == 'aci-png':
    filename = f'{taskid}-nodes.png'
  elif f == 'aci-pdb':
    filename = f'{taskid}-bfactor.pdb'
  elif f == 'pdb':
    filename = f'{taskid}.pdb'
  elif f == 'probability':
    filename = f'{taskid}.mat'
  elif f == 'ind':
    filename = f'{taskid}.ind'

  r = ftp_fetch(f'ohm/{taskid}', filename)

  if download_name == '':
    download_name = filename

  return send_file(r, download_name=download_name, as_attachment=True)

@app.route('/ohm/contact', methods=['POST'])
@cross_origin(supports_credentials=True)
def ohm_contact():
  par = request.files['par'].read()
  par = json.loads(par)
  title = par['title']
  message = par['content']
  user = par['user']
  email = par['email']

  print(title, message, user, email)

  msg = Message(subject=title, sender="dokhlab@outlook.com", recipients=['juw1179@psu.edu'], body='{0}<{1}>:{2}'.format(user, email, message))
#  msg.msgId = msg.msgId.split('@')[0] + '@short_string'
  mail.send(msg)
  return 'done'

@app.route('/ohm/submit', methods=['POST'])
@cross_origin(supports_credentials=True)
def ohm_submit():
  task_id = random.randrange(999999999)
  userid = 0
  task_level = 0
  if 'user' in session:
    user = session['user']
    userid = user['id']
    tasklevel = 1
  par = request.files['par'].read()
  title = json.loads(par)['title']

  with open_db() as dokhlab_cursor:
    dokhlab_cursor.execute("insert into ohm_tasks (tsubmit, id, userid, title, par, level) values(CURRENT_TIMESTAMP(), %s, %s, %s, %s, %s)", (task_id, userid, title, par, task_level))

    return str(task_id);

@app.route('/search_rcsb', methods=['GET'])
@cross_origin(supports_credentials=True)
def search_rcsb():
  query = request.args.get('query')

  url = 'https://search.rcsb.org/rcsbsearch/v2/query'

  jsonstr = """{
    "query": {
      "type": "terminal",
      "service": "full_text",
      "parameters": {
        "value": "%s"
      }
    },
    "return_type": "entry"
  }""" % (query,)

  r = requests.get(url, params={'json':jsonstr})
  r = r.json()

  pdbs = [i['identifier'] for i in r['result_set']]

  jsonstr = """{
    entries(entry_ids:[%s]) {
      rcsb_id
      struct {
        pdbx_descriptor
        title
      }
    }
  }""" % (','.join(f'"{i}"' for i in pdbs))

  url = 'https://data.rcsb.org/graphql'

  r = requests.get(url, params={'query':jsonstr})
  r = r.json()

  return r

@app.route('/inventory/username', methods=['GET'])
@cross_origin(supports_credentials=True)
def inventory_username():
  user_id = request.args.get('userid')
  with open_db() as cursor:
    cursor.execute('select username from users where id=%s', (user_id,))
    r = cursor.fetchall()
  return r[0]

@app.route('/inventory/history', methods=['GET'])
@cross_origin(supports_credentials=True)
def inventory_history():
  user_id = request.args.get('table')
  user_id = request.args.get('id')
  with open_db() as cursor:
    cursor.execute('select * from history where table_name=%s AND record_id=%s', (table_name, record_id))
    r = cursor.fetchall()
    return json.dumps(r, default=json_default)

@app.route('/inventory/download')
@cross_origin(supports_credentials=True)
def inventory_download():
  name = request.args.get('name')
  download_name = '{0}'.format(name)
  r = ftp_fetch('inventory', download_name)
  mimetype = mimetypes.guess_type(download_name, False)[0]
  return send_file(r, download_name=os.path.basename(download_name), mimetype=mimetype)

@app.route('/inventory/del')
@cross_origin(supports_credentials=True)
def inventory_del():
  content = json.load(request.files['file'])
  for k,v in content.items():
    if k == '__table':
      table = v;
    else:
      keys.append(k)
      values.append(v)
  with open_db() as cursor:
    placeholder = ' and '.join(k+'=%s' for k in keys)
    cursor.execute(f'delete from {table} where {placeholder}', values)
    return 'done'

@app.route('/inventory/edit')
@cross_origin(supports_credentials=True)
def inventory_edit():
  content = json.load(request.files['file'])
  for k,v in content.items():
    if k == '__table':
      table = v
    elif k == '__key':
      key = v

  for k,v in content.items():
    if k != '__table' and k != '__key':
      if k == 'password' or k == 'passwd':
        v = md5(v)
      if k == key:
        key_value = v
      else:
        keys.append(k)
        values.append(v)

  with open_db() as cursor:
    placeholder = ', '.join(k+'=%s' for k in keys)
    cursor.execute(f'update {table} set {placeholder} where {key}={key_value}', values)
    return 'done'

# Delete file
@app.route('/inventory/delete')
@cross_origin(supports_credentials=True)
def inventory_delete():
  filename = request.args.get('name')
  ftp_delete('inventory', filename)
  return 'done'

def scan_dir(ftp, ls):
  lines = []
  ftp.dir('.', lines.append)
  for line in lines:
    permission, _, user, group, size, month, date, time, name = line.split()
    obj = {'name':name, 'sons':[]}
    if line[0] == 'd':
      ftp.cwd(name)
      scan_dir(ftp, obj['sons'])
      ftp.cwd('..')
    ls.append(obj)

# list files
@app.route('/inventory/dir')
@cross_origin(supports_credentials=True)
def inventory_dir():
  path = request.args.get('name')
  with open_ftp() as ftp:
    ls = []
    ftp.cwd(path)
    scan_dir(ftp, ls)
    return json.dumps(ls, default=json_default)

@app.route('/inventory/upload', methods=['POST'])
@cross_origin(supports_credentials=True)
def inventory_upload():
  path = request.form['path']
  f = request.files['file']
  with open_ftp() as ftp:
    ftp.cwd(path)
    filename = secure_filename(f.filename)
    ftp.storbinary(f'STOR {filename}', f)
    return 'done'

@app.route('/inventory/table-config', methods=['GET'])
@cross_origin(supports_credentials=True)
def inventory_table_config():
  filename = 'table-config.json'
  r = ftp_fetch('inventory', filename)
  return send_file(r, download_name=filename, as_attachment=True)


