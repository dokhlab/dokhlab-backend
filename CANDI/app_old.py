from flask import Flask, render_template, request, jsonify, flash, session, send_file, redirect
import pandas as pd
import os
import csv
from werkzeug.utils import secure_filename
from rdkit import Chem
from rdkit.Chem import AllChem, DataStructs
import pandas as pd
from itertools import combinations
import numpy as np 
import reactome2py as reactome

from itertools import combinations
from flask import jsonify
import reactome2py
from reactome2py import analysis

from flask import jsonify
from flask import url_for

import requests
from flask import send_from_directory
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import io
import base64
import builtins
from builtins import zip
# Get the current directory
current_dir = os.path.dirname(os.path.realpath(__file__))

app = Flask(__name__)
app.secret_key = 'your_secret_key'

UPLOAD_FOLDER = os.path.join(current_dir, 'uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Ensure the upload folder exists
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

# Get the current directory
current_dir = os.path.dirname(os.path.realpath(__file__))

# Construct paths based on the current directory
excel_file_path = os.path.join(current_dir, 'static', 'compound_names.xlsx')
csv_file_path = os.path.join(current_dir, 'static', 'drift_pathway_only_protein.csv')

# Load data at the start of the application
excel_data = pd.read_excel(excel_file_path)
csv_data = pd.read_csv(csv_file_path)

# Define the function to fetch column names and their categories
def get_column_categories(excel_file):
    try:
        df = pd.read_excel(excel_file)
        # Assuming the Excel file has columns 'category' and 'compoundname'
        column_categories = df.groupby('category')['compoundname'].apply(list).to_dict()
        return column_categories
    except Exception as e:
        print("Error:", e)
        return None




# @app.route('/download_predictions')
# def download_predictions():
#     try:
#         # Define the file path for predicted targets file
#         file_path = os.path.join(current_dir, 'static', 'predicted_targets.txt')

#         # Send the file for download
#         return send_file(file_path, as_attachment=True)
#     except Exception as e:
#         print("Error occurred while downloading predictions:", e)
#         return "Error occurred while downloading predictions", 500

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/home')
def home():
    return render_template('index.html')

@app.route('/compounds')
def compounds():
    return render_template('compound.html')

@app.route('/search/<query>')
def search_csv(query):
    results = []
    column_names = None

    with open(os.path.join(current_dir, 'static', 'drift_pathway_only_protein.csv'), newline='') as csvfile:
        reader = csv.reader(csvfile)
        column_names = next(reader)  # Get column names

        for row in reader:
            if query.upper() in row[0].upper() or query.upper() in row[column_names.index('Compounds SMILES')].upper():  # Check both columns
                results.append({key: value for key, value in zip(column_names, row)})

    return jsonify({'data': results, 'column_names': column_names})


@app.route('/compound')
def compound():
    return render_template('index2.html')

# Replace with the actual path to your CSV file
csv_file_path = os.path.join(current_dir, 'static', 'drift_pathway_only_protein.csv')
import plotly.graph_objects as go
# Load CSV data into memory
data = pd.read_csv(csv_file_path)

smiles_path = os.path.join(current_dir, 'static', 'pubchem_id.csv')
smiles_pubchem = pd.read_csv(smiles_path)  # Replace 'smiles_pubchem.csv' with the path to your SMILES and PubChem ID CSV file

@app.route('/autocomplete', methods=['GET'])
def autocomplete():
    try:
        search_input = request.args.get('q', '').lower()  # Get the search input from the query parameter

        # Filter data based on search input
        matching_compounds = smiles_pubchem[smiles_pubchem['compound'].str.lower().str.contains(search_input)]

        # Return the list of matching compounds as JSON
        suggestions = matching_compounds['compound'].tolist()
        return jsonify(suggestions)

    except Exception as e:
        return jsonify({'error': str(e)}), 500
    
# Define a custom Jinja filter to split a string into a list
def split_string(string, delimiter=','):
    return string.split(delimiter)

# Add the custom filter to the Jinja environment
app.jinja_env.filters['split'] = split_string
@app.route('/search', methods=['POST'])
def search_data():
    try:
        search_input = request.get_json()['searchTerm'].lower()  # Convert search input to lowercase

        # Check if search input is a compound name
        compound_match = data[data['compound'].str.lower() == search_input]

        if not compound_match.empty:
            # Search term is a compound name
            search_term = search_input
        else:
            # Check if search input is a SMILES or PubChem ID
            smiles_match = smiles_pubchem[smiles_pubchem['SMILES'].str.lower() == search_input]
            if smiles_match.empty:
                try:
                    search_input_int = int(search_input)
                    pubchem_match = smiles_pubchem[smiles_pubchem['PubChem_CID'] == search_input_int]
                    if not pubchem_match.empty:
                        # Match found for PubChem ID
                        search_term = pubchem_match.iloc[0]['compound']
                    else:
                        return jsonify({'error': 'No data found for the provided search input.'}), 404
                except ValueError:
                    return jsonify({'error': 'No data found for the provided search input.'}), 404
            else:
                # Match found for SMILES
                search_term = smiles_match.iloc[0]['compound']

        # Filter data based on compound name
        filtered_data = data[data['compound'].str.lower() == search_term.lower()]

        # Sort the filtered data by scores in descending order
        filtered_data = filtered_data.sort_values(by='Score', ascending=False)
        filtered_data = filtered_data.fillna('')
        targets = filtered_data['Target'].tolist()
        scores = filtered_data['Score'].tolist()

        # Create the bar graph using Plotly
        fig = go.Figure(data=[go.Bar(x=targets, y=scores)])
        fig.update_layout(title='Target vs. Score for Compound: ' + search_term,
                          xaxis_title='Target', yaxis_title='Score')
        graph_json = fig.to_json()

        # Prepare data for table
        table_columns = ['compound','Target', 'UniProt ID', 'Score', 
                         'Most Similar Compound', 'Compounds SMILES', 
                         'Compound FP2 Similarity', 'Compound Pharmacophore Similarity', 
                         'Assay Value', 'Assay Type']
        table_data = filtered_data[table_columns].to_dict('records')

        return jsonify({'targets': targets, 'scores': scores, 'graph': graph_json, 'table': table_data})

    except Exception as e:
        return jsonify({'error': str(e)}), 500




@app.route('/pathways')
def pathways():
    return render_template('pathways.html')

# Define the function for prediction (unchanged)# Define the function for prediction (unchanged)# Define the function for prediction (unchanged)
def predict_target(csv_file, column_values):
    df = pd.read_csv(csv_file)
    target_names = df.iloc[:, 0]
    missing_columns = [col for col in column_values.keys() if col not in df.columns]
    if missing_columns:
        raise ValueError(f"Column(s) {', '.join(missing_columns)} not found in the CSV file.")
    
    # Calculate scores
    multiplied_values = df[column_values.keys()].mul(column_values.values(), axis=1)
    total_scores = multiplied_values.sum(axis=1)
    
    # Normalize scores
    normalized_scores = total_scores / total_scores.sum() if total_scores.sum() != 0 else total_scores
    
    # Sort targets based on scores
    sorted_indices = normalized_scores.argsort()[::-1]
    sorted_targets = target_names[sorted_indices]
    sorted_scores = normalized_scores[sorted_indices]
    
    # Filter out targets with 0 probability
    non_zero_indices = sorted_scores != 0
    sorted_targets = sorted_targets[non_zero_indices]
    sorted_scores = sorted_scores[non_zero_indices]

    return sorted_targets, sorted_scores

from collections import OrderedDict

from collections import OrderedDict
import csv
from scipy.stats import binom
import re

# def save_predictions_to_csv(predictions, file_path):
#     with open(file_path, 'w', newline='') as csvfile:
#         writer = csv.writer(csvfile)

#         # Write header row
#         writer.writerow(['Formulation', 'Predicted Targets'])

#         for formulation, targets in predictions.items():
#             # Use OrderedDict to maintain the order of predicted targets
#             ordered_targets = OrderedDict(targets)
#             # Write each formulation and its targets in a separate row
#             for target in ordered_targets.values():
#                 writer.writerow([formulation, target])
def save_predictions_to_csv(predictions, file_path):
    with open(file_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        # Write header row
        writer.writerow(['Formulation', 'Predicted Target', 'scores'])

        for formulation, prediction_info in predictions.items():
            targets = prediction_info['Targets']
            scores = prediction_info['Scores']
            
            # Write each target along with its formulation
            for target, score in zip(targets, scores):
                writer.writerow([formulation, target, score])

def save_pathway_results_to_csv(pathway_results, file_path):
    with open(file_path, 'w', newline='') as csvfile:
        fieldnames = ['Formulation', 'Pathway ID', 'Pathway Name', 'Mapped Entities', 'Total Entities', 'Score', 'P-value', 'FDR']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for formulation, pathways in pathway_results.items():
            writer.writerow({'Formulation': formulation})  # Write a blank row to separate formulations
            for pathway in pathways:
                row = {
                    'Formulation': formulation,
                    'Pathway ID': pathway['pathway_id'],
                    'Pathway Name': pathway['pathway_name'],
                    'Mapped Entities': pathway['mapped_entities'],
                    'Total Entities': pathway['total_entities'],
                    'Score': pathway['normalized_score'],
                    'P-value': pathway['p_value'],
                    # 'FDR': pathway['fdr']
                }
                writer.writerow(row)
            writer.writerow({})  # Write a blank row to separate formulations

@app.route('/formulation', methods=['GET', 'POST'])
def formulation():
    csv_file = os.path.join(current_dir, 'static', 'combined_only_protein.csv')
    uniprot_id_mapping_file = os.path.join(current_dir, 'static', 'uniport_id_mapping_only_protein.csv')

    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file:
            csv_filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], csv_filename)
            file.save(file_path)
            df = pd.read_csv(file_path)
            try:
                df = pd.read_csv(file_path)
                
                # Check if the dataframe has the required structure
                if df.shape[1] < 2:
                    raise ValueError("The uploaded file does not have the required structure.")
                
                # Extract column values for each formulation
                column_values = {}
                for col in df.columns[1:]:
                    column_values[col] = dict(zip(df.iloc[:, 0], df[col]))
                    
                # Predict targets for each set of column values
                all_predictions = {}
                for formulation, values in column_values.items():
                    predicted_targets, predicted_scores = predict_target(csv_file, values)
                    all_predictions[formulation] = {'Targets': predicted_targets, 'Scores': predicted_scores}
                # print(all_predictions[formulation])
                # Map predicted targets to UniProt IDs
                uniprot_id_mapping = pd.read_csv(uniprot_id_mapping_file, index_col='Target Name')['UniProt_ID'].to_dict()
                all_pathway_results = {}

                # for formulation, prediction_info in all_predictions.items():
                #     targets = prediction_info['Targets']
                #     scores = prediction_info['Scores'].tolist()
                #     print(targets, scores)
                    

                #     uniprot_ids_with_scores = [(uniprot_id_mapping.get(target, ''), score) for target, score in zip(targets, scores) if uniprot_id_mapping.get(target)]
                #     for target, score in zip(targets.tolist(), scores):
                #         uniprot_ids_with_scores.append((uniprot_id_mapping.get(target, ''), score))
                #     all_pathway_results[formulation] = uniprot_ids_with_scores
                for formulation, prediction_info in all_predictions.items():
                    targets = prediction_info['Targets']
                    scores = prediction_info['Scores'].tolist()

                    # Initialize the list to store UniProt IDs with scores
                    uniprot_ids_with_scores = []

                    # Iterate over targets and scores simultaneously
                    for target, score in zip(targets, scores):
                        # Check if the target has a corresponding UniProt ID in the mapping
                        uniport_id = uniprot_id_mapping.get(target, '')
                        if uniport_id:
                            print(uniport_id)
                            # Append the UniProt ID and score to the list
                            uniprot_ids_with_scores.append((uniport_id, score))
                            
                    # Update the pathway results dictionary with UniProt IDs and scores
                    all_pathway_results[formulation] = uniprot_ids_with_scores
                    # print(all_pathway_results[formulation])
                    

                    # Retrieve UniProt IDs and their scores from all pathway results
                    user_genes = set()
                    uniport_weights = {}
                    for formulation, uniprot_ids_with_scores in all_pathway_results.items():
                        for entry in uniprot_ids_with_scores:
                            # Check if the entry has exactly two elements
                            if len(entry) == 2:
                                uniprot_id, score = entry
                                user_genes.add(uniprot_id)
                                if uniprot_id not in uniport_weights:
                                    uniport_weights[uniprot_id] = score
                                else:
                                    uniport_weights[uniprot_id] += score
                            else:
                                print(f"Ignoring entry with invalid format: {entry}")


                    # Function to calculate binomial p-value
                    def calculate_pvalue(pathway_genes, all_genes, user_genes):
                        M = len(all_genes)  # Total number of genes considered
                        K = len(user_genes)  # Number of user-provided genes
                        x = len(set(pathway_genes) & user_genes)  # Number of user genes in the pathway
                        
                        # Calculate the binomial p-value
                        pvalue = 1 - binom.cdf(x - 1, K, len(pathway_genes) / M)
                        # Convert p-value to scientific notation with 2 digits after the decimal point
                        pvalue = "{:.2e}".format(pvalue)
                        return pvalue
                    # Load the CSV file
                    # df = pd.read_csv('/home/srinivasan/Downloads/UniProt2Reactome_All_Levels.txt', sep='\t', header=None, names=['UniProt ID', 'Pathway ID', 'Pathway URL', 'Pathway Name', 'Evidence Code', 'Species'])
                    # df = pd.read_csv('/home/srinivasan/Downloads/UniProt2Reactome_subpathways.txt', sep='\t', header=None, names=['UniProt ID', 'Pathway ID', 'Pathway URL', 'Pathway Name', 'Evidence Code', 'Species'])
                    reactome_file = os.path.join(current_dir, 'static', 'UniProt2Reactome_PE_Pathway.txt')
                    df = pd.read_csv(reactome_file, sep='\t', header=None, names=['UniProt ID','part', 'gene', 'Pathway ID', 'Pathway URL', 'Pathway Name', 'Evidence Code', 'Species'])

                    # Filter for Homo sapiens species
                    df = df[df['Species'] == 'Homo sapiens']
                    # Get the list of all UniProt IDs
                    all_genes = set(df['UniProt ID'].unique())

                    # Create a dictionary to store pathway information
                    pathway_info = {}

                    # Iterate over each pathway
                    for pathway_id, pathway_name in zip(df['Pathway ID'].unique(), df['Pathway Name'].unique()):
                        pathway_genes = set(df.loc[df['Pathway ID'] == pathway_id, 'UniProt ID'])
                        mapped_entities = len(pathway_genes & user_genes)
                        total_entities = len(pathway_genes)
                        
                        # Calculate pathway score based on mapped entities and UniProt ID weights
                        score = sum(uniport_weights.get(gene, 0) for gene in pathway_genes) * mapped_entities / len(user_genes) if len(user_genes) > 0 else 0
                        
                        # Check if the intersection between pathway genes and user genes is non-empty
                        if mapped_entities > 0:
                            pvalue = calculate_pvalue(pathway_genes, all_genes, user_genes)
                            pathway_info[pathway_id] = {'Pathway Name': pathway_name, 'p-value': pvalue, 'Mapped Entities': mapped_entities, 'Total Entities': total_entities, 'Score': score}

                    # Normalize pathway scores using min-max normalization
                    # min_score = min(info['Score'] for info in pathway_info.values())
                    # max_score = max(info['Score'] for info in pathway_info.values())
                    # for info in pathway_info.values():
                    #     info['Normalized Score'] = (info['Score'] - min_score) / (max_score - min_score) if max_score != min_score else 0
                    # Normalize pathway scores using min-max normalization to range between 0.1 and 0.9
                    min_score = min(info['Score'] for info in pathway_info.values())
                    max_score = max(info['Score'] for info in pathway_info.values())
                    range_min = 0.1
                    range_max = 0.9

                    for info in pathway_info.values():
                        normalized_score = ((info['Score'] - min_score) / (max_score - min_score)) * (range_max - range_min) + range_min if max_score != min_score else 0.5
                        info['Normalized Score'] = round(normalized_score, 2)
                    # Sort the pathways based on normalized score
                    sorted_pathways = sorted(pathway_info.items(), key=lambda x: x[1]['Normalized Score'], reverse=True)

                    pathway_results = []
                    for pathway_id, info in sorted_pathways:
                        pathway_name = info['Pathway Name']
                        mapped_entities = info['Mapped Entities']
                        total_entities = info['Total Entities']
                        p_value = info['p-value']
                        # fdr = info['fdr']
                        normalized_score = info['Normalized Score']

                        # Construct the pathway diagram filename
                        diagram_filename = f"{pathway_id}.png"
                        diagram_path = os.path.join(current_dir, 'static', 'diagram', diagram_filename)

                        # Check if the diagram file exists
                        if os.path.exists(diagram_path):
                            # Provide link to saved pathway diagram along with pathway name in the JSON response
                            pathway_info = {
                                'pathway_id': pathway_id,
                                'pathway_name': pathway_name,
    #                            'diagram_url': url_for('serve_pathway_diagram', filename=diagram_filename),
                                'diagram_url': 'pathway_diagrams/'+diagram_filename,
                                'mapped_entities': mapped_entities,
                                'total_entities': total_entities,
                                'p_value': p_value,
                                'normalized_score': normalized_score,
                                'pathway_genes': pathway_genes,
                            }
                            pathway_results.append(pathway_info)

                    all_pathway_results[formulation] = pathway_results

                # Save predictions to a single CSV file
                predictions_file_path = os.path.join(current_dir, 'static', 'predicted_targets.csv')
                save_predictions_to_csv(all_predictions, predictions_file_path)
                # Call the function to save the pathway results
                pathway_results_file_path = os.path.join(current_dir, 'static', 'pathway_results.csv')
                save_pathway_results_to_csv(all_pathway_results, pathway_results_file_path)
                return render_template('results4_layout2.html', predictions=all_predictions, pathway_results=all_pathway_results, uniprot_id_mapping=uniprot_id_mapping, zip=zip)
            except (pd.errors.ParserError, ValueError) as e:
                flash(f"Error processing file: {str(e)}. Please use the downloaded sample file format.")
                return jsonify({'error': 'No data found for the provided search input.'}), 404
            except Exception as e:
                flash(f"Error processing file. Please use the downloaded sample file format.")
                return redirect(request.url)
            

    return render_template('index5.html')
from flask import make_response
import csv
import io

@app.route('/download_sample_csv')
def download_sample_csv():
    # Define the path to your sample CSV file
    sample_csv_path = os.path.join(current_dir, 'static', 'file.csv')  # Update this with the actual path to your sample CSV file

    # Define the filename for the downloaded file
    filename = 'sample.csv'

    # Send the file to the client for download
    return send_file(sample_csv_path, as_attachment=True)


@app.route('/download_formulation_results/<formulation>', methods=['GET'])
def download_formulation_results(formulation):
    predictions_file_path = os.path.join(current_dir, 'static', 'predicted_targets.csv')
    return send_file(predictions_file_path, as_attachment=True)

@app.route('/download_pathway_results/<formulation>', methods=['GET'])
def download_pathway_results(formulation):
    pathway_results_file_path = os.path.join(current_dir, 'static', 'pathway_results.csv')
    return send_file(pathway_results_file_path, as_attachment=True)


@app.route('/pathway_diagrams/<path:filename>')
def serve_pathway_diagram(filename):
    return send_from_directory(os.path.join(current_dir, 'static', 'diagram'), filename)

# @app.route('/search-pathways', methods=['POST'])
# def results():
#     if request.method == 'POST':
#         uniprot_ids_input = request.form.get('uniprot_ids')

#         # Count the total number of user-provided UniProt IDs
#         uniprot_ids = [id.strip() for id in uniprot_ids_input.replace(',', '\n').split('\n')]
#         num_user_ids = len(uniprot_ids)

#         # Perform Reactome analysis for all UniProt IDs together
#         all_uniprot_ids = ','.join(uniprot_ids)
#         token = analysis.identifiers(ids=all_uniprot_ids.strip(), interactors=False, page_size='2000', page='1', species='Homo Sapiens', sort_by='ENTITIES_FDR', order='ASC', resource='TOTAL', p_value='1', include_disease=True, min_entities=None, max_entities=None, projection=False)
#         result = token

#         pathway_results = []
#         for pathway in result['pathways']:
#             pathway_id = pathway['stId']
#             pathway_name = pathway['name']
#             entities = pathway['entities']
#             total_entities = pathway['entities']['total']
#             mapped_entities = pathway['entities']['found']
#             p_value = pathway['entities']['pValue']
#             fdr = pathway['entities']['fdr']

#             # Calculate the score
#             score = mapped_entities / num_user_ids

#             # Construct the pathway diagram filename
#             diagram_filename = f"{pathway_id}.png"
#             diagram_path = os.path.join('/home/srinivasan/Desktop/CANDI/static/diagram/', diagram_filename)

#             # Check if the diagram file exists
#             if os.path.exists(diagram_path):
#                 # Provide link to saved pathway diagram along with pathway name in the JSON response
#                 pathway_info = {
#                     'pathway_id': pathway_id,
#                     'pathway_name': pathway_name,
#                     'diagram_url': url_for('serve_pathway_diagram', filename=diagram_filename),
#                     'mapped_entities': mapped_entities,
#                     'total_entities': total_entities,
#                     'score': score,
#                     'p_value': p_value,
#                     'fdr': fdr
#                 }
#                 pathway_results.append(pathway_info)

#         # Return the results in JSON format
#         return jsonify(pathway_results)
#     # Handle other HTTP methods gracefully
#     return redirect(url_for('index5')) 





















# @app.route('/search-pathways', methods=['POST'])
# def results():
#     if request.method == 'POST':
#         uniprot_ids_input = request.form.get('uniprot_ids')
        
#         # Count the total number of user-provided UniProt IDs
#         uniprot_ids = [id.strip() for id in uniprot_ids_input.replace(',', '\n').split('\n')]
#         num_user_ids = len(uniprot_ids)

#         # Perform Reactome analysis for all UniProt IDs together
#         all_uniprot_ids = ','.join(uniprot_ids)
#         token = analysis.identifiers(ids=all_uniprot_ids.strip(), interactors=False, page_size='2000', page='1', species='Homo Sapiens', sort_by='ENTITIES_FDR', order='ASC', resource='TOTAL', p_value='1', include_disease=True, min_entities=None, max_entities=None, projection=False)
#         result = token

#         pathway_results = []
#         for pathway in result['pathways']:
#             pathway_id = pathway['stId']
#             pathway_name = pathway['name']
#             entities = pathway['entities']
#             total_entities = pathway['entities']['total']
#             mapped_entities = pathway['entities']['found']
#             p_value = pathway['entities']['pValue']
#             fdr = pathway['entities']['fdr']

#             # Calculate the score
#             score = mapped_entities / num_user_ids

#             # Store pathway information in a dictionary
#             pathway_info = {'pathway_id': pathway_id,'pathway_name': pathway_name, 'mapped_entities': mapped_entities, 'total_entities': total_entities, 'score': score, 'p_value': p_value, 'fdr': fdr}
#             pathway_results.append(pathway_info)

#         # Return the results in JSON format
#         return jsonify(pathway_results)



#@app.route('/home')
#def home():
#   return render_template('layout.html')

# @app.route('/plotting')
# def plotting():
#     return render_template('plot_matrix.html')



# @app.route('/plotting', methods=['GET', 'POST'])
# def plotting():
#     if request.method == 'POST':
#         # Get the user input
#         compounds = request.form.get('compounds').split(',')

#         # Load the data
#         path_to_csv = "similarity_matrix.csv"
#         data = pd.read_csv(path_to_csv, index_col=0)

#         # Find the optimal order that maximizes the sum of adjacent values
#         row_order = data.sum(axis=1).sort_values(ascending=False).index
#         col_order = data.loc[row_order].sum(axis=0).sort_values(ascending=False).index

#         # Reorder rows and columns based on the optimal order
#         data = data.loc[row_order, col_order]

#         # Determine the teal and green labels based on the user input
#         teal_labels = [label for label in compounds if label in data.columns]
#         green_labels = [label for label in compounds if label in data.index]

#         # Create a new DataFrame with only the user-provided compounds
#         new_data = data.loc[green_labels, teal_labels]

#         # Create the plot
#         fig, (ax_heatmap, ax_cbar) = plt.subplots(1, 2, figsize=(12, 12), gridspec_kw={'width_ratios': [10, 1]})
#         clustered_heatmap = sns.heatmap(new_data, cmap='coolwarm', ax=ax_heatmap, cbar_ax=ax_cbar)

#         # Change the color of specific x-axis tick labels
#         for tick_label in ax_heatmap.get_xticklabels():
#             if tick_label.get_text() in teal_labels:
#                 tick_label.set_color('teal')
#             else:
#                 tick_label.set_color('black')

#         # Change the color of specific y-axis tick labels
#         for tick_label in ax_heatmap.get_yticklabels():
#             if tick_label.get_text() in green_labels:
#                 tick_label.set_color('green')
#             else:
#                 tick_label.set_color('black')

#         # Create legend patches
#         red_patch = mpatches.Patch(color='green', label='Cannabinoids')
#         green_patch = mpatches.Patch(color='teal', label='Flavonoids')
#         black_patch = mpatches.Patch(color='black', label='Terpenes')

#         # Add legend adjacent to the heatmap
#         legend_ax = fig.add_axes([ax_heatmap.get_position().x1 + 0.01, ax_heatmap.get_position().y0, 0.1, ax_heatmap.get_position().height])
#         legend_ax.legend(handles=[red_patch, green_patch, black_patch], loc='upper right')
#         legend_ax.axis('off')

#         # Save the plot to a buffer
#         buf = io.BytesIO()
#         plt.savefig(buf, format='png')
#         plot_url = base64.b64encode(buf.getvalue()).decode('utf-8')

#         return render_template('plot_matrix.html', plot_url=plot_url)

#     return render_template('plot_matrix.html')



def generate_heatmap(compounds, csv_filename):
    # Your existing code for generating heatmap
    # Load the similarity matrix from the provided CSV file
    data = pd.read_csv(csv_filename, index_col=0)
    
    # Normalize the columns and index by stripping leading/trailing spaces and converting to lowercase
    data.columns = data.columns.str.strip().str.lower()
    data.index = data.index.str.strip().str.lower()
    
    # Normalize the input compounds by stripping leading/trailing spaces and converting to lowercase
    normalized_compounds = [compound.strip().lower() for compound in compounds]
    
    # Ensure compounds are found in the similarity matrix
    compounds_in_data = [compound for compound in normalized_compounds if compound in data.columns and compound in data.index]
    compounds_not_found = [compound for compound in normalized_compounds if compound not in compounds_in_data]

    # Log compounds not found
    if compounds_not_found:
        print(f"Compounds not found: {compounds_not_found}")

    if not compounds_in_data:
        raise ValueError(f"None of the provided compounds are found in the similarity matrix. Compounds not found: {compounds_not_found}")

    # Define labels and their corresponding colors
    teal_labels = ['kaempferol', 'luteolin', 'vitexin', 'rutin', 'chrysin', 'baicalin',
                   'orientin', 'quercetin-3-glucoside', 'isovexitin', 'luteolin-7-o-glucoside',
                   'apigenin-7-glucoside', 'catechin', 'quercetin', 'epicatechin',
                   'epigalocatechin', 'cannflavin b', 'b-sitosterol', 'cannflavin a']
    green_labels = ['cannabichromene', 'cannabichromenic acid', 'cannabidiol (cbd)',
                    'cannabidiolic acid (cbda)', 'cannabidivarin (cbdv)', 'cannabidivarinic acid (cbdva)',
                    'cannabigerol (cbg)', 'cannabigerolic acid (cbga)', 'cannabicyclolic acid (cbla)',
                    'cannabinol (cbn)', 'cannabinolic acid (cbna)', 'delta8-tetrahydrocannabinol (d8-thc)',
                    'tetrahydrocannabinolic acid (thca)', 'tetrahydrocannabivarin (thcv)',
                    'tetrahydrocannabivarinic acid (thcva)', 'delta9-tetrahydrocannabinol (d9-thc)']

    # Create a new DataFrame with only the compounds present in the data
    new_data = data.loc[compounds_in_data, compounds_in_data]

    # Create the plot
    fig, (ax_heatmap, ax_cbar) = plt.subplots(1, 2, figsize=(16, 12), gridspec_kw={'width_ratios': [10, 1]})
    clustered_heatmap = sns.heatmap(new_data, cmap='coolwarm', ax=ax_heatmap, cbar_ax=ax_cbar, cbar_kws={'orientation': 'vertical'})

    # Change the color of specific x-axis tick labels
    for tick_label in ax_heatmap.get_xticklabels():
        if tick_label.get_text() in teal_labels:
            tick_label.set_color('teal')
        elif tick_label.get_text() in green_labels:
            tick_label.set_color('green')
        else:
            tick_label.set_color('black')

    # Change the color of specific y-axis tick labels
    for tick_label in ax_heatmap.get_yticklabels():
        if tick_label.get_text() in green_labels:
            tick_label.set_color('green')
        elif tick_label.get_text() in teal_labels:
            tick_label.set_color('teal')
        else:
            tick_label.set_color('black')

    # Adjust the layout to ensure the labels are fully visible
    plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)

    # Create legend patches
    green_patch = mpatches.Patch(color='green', label='Cannabinoids')
    teal_patch = mpatches.Patch(color='teal', label='Flavonoids')
    black_patch = mpatches.Patch(color='black', label='Terpenes')

    # Add legend above the heatmap
    legend_ax = fig.add_axes([ax_heatmap.get_position().x0, ax_heatmap.get_position().y1 + 0.02, ax_heatmap.get_position().width, 0.05])
    legend_ax.legend(handles=[green_patch, teal_patch, black_patch], loc='upper center', bbox_to_anchor=(0.5, 0.3), ncol=3)
    legend_ax.axis('off')

    # Save the plot to a file
    plot_filename = os.path.basename(csv_filename).replace('.csv', '_heatmap_plot.png')
    plot_path = os.path.join(current_dir, 'static', plot_filename)
    plt.savefig(plot_path, format='png', bbox_inches='tight', dpi=600)
    plt.close()  # Close the plot to release resources

    return plot_filename


def generate_csv(compounds, csv_filename):
    # Load the similarity matrix from the provided CSV file
    data = pd.read_csv(csv_filename, index_col=0)

    # Select only the rows and columns corresponding to the provided compounds
    selected_data = data.loc[compounds, compounds]

    # Save the selected data to a CSV file
    csv_output_filename = os.path.basename(csv_filename).replace('.csv', '_selected_data.csv')
    csv_path = os.path.join(current_dir, 'static', csv_output_filename)
    selected_data.to_csv(csv_path, index=True)

    return csv_output_filename


@app.route('/plotting', methods=['GET', 'POST'])
def plotting():
    if request.method == 'POST':
        compounds_input = request.form['compounds']
        if compounds_input.strip() == '':
            error_message = "Please enter at least one compound."
            return render_template('plot_matrix.html', error_message=error_message, plot_filename1=None, plot_filename2=None, csv_filename1=None, csv_filename2=None)
        
        compounds = [compound.strip() for line in compounds_input.split('\n') for compound in line.split(',')]
        
        path_to_csv1 = os.path.join(current_dir, 'static', 'similarity_matrix_only_protein.csv')
        path_to_csv2 = os.path.join(current_dir, 'static', 'similarity_matrix_pathways_only_protein.csv')
        
        try:
            plot_filename1 = generate_heatmap(compounds, path_to_csv1)
            plot_filename2 = generate_heatmap(compounds, path_to_csv2)
        except ValueError as e:
            error_message = str(e)
            return render_template('plot_matrix.html', error_message=error_message, plot_filename1=None, plot_filename2=None, csv_filename1=None, csv_filename2=None)
        except KeyError as e:
            error_message = f"Compound not found: {str(e)}"
            return render_template('plot_matrix.html', error_message=error_message, plot_filename1=None, plot_filename2=None, csv_filename1=None, csv_filename2=None)
        
        csv_filename1 = generate_csv(compounds, path_to_csv1)
        csv_filename2 = generate_csv(compounds, path_to_csv2)
        
        return render_template('plot_matrix.html', plot_filename1=plot_filename1, plot_filename2=plot_filename2, csv_filename1=csv_filename1, csv_filename2=csv_filename2)
    
    return render_template('plot_matrix.html', plot_filename1=None, plot_filename2=None, csv_filename1=None, csv_filename2=None)


@app.route('/download_plot')
def download_plot():
    plot_filename = request.args.get('plot_filename')
    if plot_filename:
        plot_path = os.path.join(current_dir, 'static', plot_filename)
        return send_file(
            plot_path,
            mimetype='image/png',
            as_attachment=True,
            download_name=plot_filename
        )
    else:
        return "Plot not available for download."


@app.route('/download_csv')
def download_csv():
    csv_filename = request.args.get('csv_filename')
    if csv_filename:
        csv_path = os.path.join(current_dir, 'static', csv_filename)
        return send_file(
            csv_path,
            mimetype='text/csv',
            as_attachment=True,
            download_name=csv_filename
        )
    else:
        return "CSV file not available for download."
    
@app.route('/help')
def help():
    return render_template('help.html')

@app.route('/about')
def about():
    return render_template('about.html')

if __name__ == '__main__':
    app.run(debug=True)
