function search() {
    var input, filter;
    input = document.getElementById("searchInput");
    filter = input.value.toUpperCase();

    // Clear previous search results
    var table = document.getElementById("results-container");
    table.innerHTML = "";

    // Send AJAX request to Flask route
    fetch(`/search/${filter}`)
    .then(response => response.json())
    .then(data => {
        if (data.data.length > 0) {
            // Extract column names and data
            const column_names = data.column_names;  // Access column names
            const resultsData = data.data;  // Access filtered data

            // Create table element
            const dataTable = document.createElement("table");
            dataTable.classList.add("data-table"); // Add a class for styling

            // Create table headers using column names
            const headerRow = document.createElement("tr");
            column_names.forEach(header => {
                const th = document.createElement("th");
                th.textContent = header;
                headerRow.appendChild(th);
            });
            dataTable.appendChild(headerRow);

            // Create table rows for data
            resultsData.forEach(rowData => {
                const row = document.createElement("tr");
                column_names.forEach(header => {
                    const cell = document.createElement("td");
                    cell.textContent = rowData[header];  // Access data by column name
                    row.appendChild(cell);
                });
                dataTable.appendChild(row);
            });

            // Append table to results container
            table.appendChild(dataTable);
        } else {
            // Display message if no results found
            // ... (code for no results message)
        }
    })
    .catch(error => console.error(error));
}
function downloadResults() {
    var query = document.getElementById("searchInput").value; // Get the search query from the input field

    // Make an AJAX request to download_search_results route
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/download_search_results/" + query, true);
    xhr.responseType = "blob";

    xhr.onload = function() {
        if (xhr.status === 200) {
            // Create a temporary anchor element to trigger the download
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(xhr.response);
            a.href = url;
            a.download = "search_results.csv";
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);
        }
    };

    xhr.send();
}