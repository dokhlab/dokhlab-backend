import os
import glob
# Get the current directory
current_dir = os.path.dirname(os.path.realpath(__file__))

def find_matching_plots(option1, option2):
    # Construct a pattern to match filenames based on selected options
    pattern = f"{option1}* vs {option2}*.png"
# def check_plot_existence(option1, option2):
#     # Construct the filename based on selected options
    plot_filename = f"{option1} vs {option2}.png"
    
    # Assuming the plots are saved in the "scatter_plots" folder inside "static"
    plot_folder = os.path.join(current_dir, 'scatter_plots')
    # Use glob to find files that match the pattern
    matching_files = glob.glob(os.path.join(plot_folder, pattern))

    # Print the matching files
    for file in matching_files:
        print(f"Matching file found: {file}")

# Example usage
option1 = "(-) α-Bisabolol"
option2 = "(1S)-(-)-Camphor"
find_matching_plots(option1, option2)


#     # Check if the plot file exists
#     plot_path = os.path.join(plot_folder, plot_filename)
    
#     # Check if the plot file exists in the folder
#     if os.path.exists(plot_path):
#         print(f"Plot '{plot_filename}' exists.")
#     else:
#         print(f"Plot '{plot_filename}' does not exist.")

# # Example usage
# option1 = "Camphene"
# option2 = "Fenchone"
# check_plot_existence(option1, option2)
